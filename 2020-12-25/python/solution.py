from typing import Iterable, List, Tuple
from itertools import islice 


modby = 20201227

def sequence(seed: int) -> Iterable[int]:
    value = 1 
    yield value
    while True:
        value = (value * seed) % modby
        yield value

def i_index(gen: Iterable[int], nums: List[int]) -> Tuple[int]:
    output = [None] * len(nums)
    for i, n in enumerate(gen):
        if n in nums:
            output[nums.index(n)] = i
            if all(n is not None for n in output):
                return tuple(output)

def iterate_n(seed, n):
    return next(islice(sequence(seed), n, n+1))

def part_one(card_pub_key: int, door_pub_key: int) -> int:
    secret = 7
    nums = sequence(secret)
    card_cycles, door_cycles = i_index(nums, [card_pub_key, door_pub_key])
    print(card_cycles, door_cycles)
    k1 = iterate_n(card_pub_key, door_cycles)
    k2 = iterate_n(door_pub_key, card_cycles)
    assert k1 == k2, "Keys dont match"
    print(k1)
    return k1

    

if __name__ == "__main__":
    print("Part One: ", part_one(8421034, 15993936))