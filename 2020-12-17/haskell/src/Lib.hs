module Lib
  ( partOne,
    partTwo,
    adjacents3,
  )
where

import Control.Monad (replicateM)
import qualified Data.Set as Set

type Plane = [[Bool]]

parse :: String -> Plane
parse = map ((== '#') <$>) . lines

type Cube = Set.Set (Int, Int, Int)

indexedMap :: (Int -> a -> b) -> [a] -> [b]
indexedMap f = zipWith f [0 ..]

gridFromPlane :: Plane -> Cube
gridFromPlane = Set.fromList . map fst . filter snd . concat . indexedMap (\y row -> indexedMap (\x on -> ((x, y, 0), on)) row)

adjacents3 :: (Int, Int, Int) -> [(Int, Int, Int)]
adjacents3 (x, y, z) =
  let move :: [Int] -> (Int, Int, Int)
      move [dx, dy, dz] = (x + dx, y + dy, z + dz)
   in map move $ filter (/= [0, 0, 0]) $ replicateM 3 [-1, 0, 1]

simulateCycle :: Cube -> Cube
simulateCycle cube =
  let isActive = (`Set.member` cube)

      countActive = length . filter isActive . adjacents3

      willBeActive :: (Int, Int, Int) -> Bool
      willBeActive cell =
        if isActive cell
          then (`elem` [2, 3]) $ countActive cell
          else 3 == countActive cell
   in Set.filter willBeActive $ Set.fromList $ concatMap adjacents3 cube

partOne :: String -> IO ()
partOne filePath = do
  contents <- readFile filePath
  putStrLn $ ("Part One: " ++) $ show $ Set.size $ (!! 6) $ iterate simulateCycle $ gridFromPlane $ parse contents

type HyperCube = Set.Set (Int, Int, Int, Int)

hyperCubeFromPlane :: Plane -> HyperCube
hyperCubeFromPlane = Set.fromList . map fst . filter snd . concat . indexedMap (\y row -> indexedMap (\x on -> ((x, y, 0, 0), on)) row)

adjacents4 :: (Int, Int, Int, Int) -> [(Int, Int, Int, Int)]
adjacents4 (x, y, z, w) =
  let move :: [Int] -> (Int, Int, Int, Int)
      move [dx, dy, dz, dw] = (x + dx, y + dy, z + dz, w + dw)
   in map move $ filter (/= [0, 0, 0, 0]) $ replicateM 4 [-1, 0, 1]

simulateHyperCycle :: HyperCube -> HyperCube
simulateHyperCycle hypercube =
  let isActive = (`Set.member` hypercube)

      countActive = length . filter isActive . adjacents4

      willBeActive :: (Int, Int, Int, Int) -> Bool
      willBeActive cell =
        if isActive cell
          then (`elem` [2, 3]) $ countActive cell
          else 3 == countActive cell
   in Set.filter willBeActive $ Set.fromList $ concatMap adjacents4 hypercube

partTwo :: String -> IO ()
partTwo filePath = do
  contents <- readFile filePath
  putStrLn $ ("Part Two: " ++) $ show $ length $ (!! 6) $ iterate simulateHyperCycle $ hyperCubeFromPlane $ parse contents