from typing import List, Dict, Tuple
from itertools import product 
from operator import itemgetter 
from collections import defaultdict 

def get_data() -> List[str]:
    with open('../2020-12-17.txt', 'r') as input_file:
        return input_file.read().split('\n')

def parse_as_grid(data : List[str]) -> List[List[bool]]:
    return [[cell == "#" for cell in row] for row in data]

CubeState = Dict[Tuple[int, int, int], bool]

def grid_to_cube(seed: List[List[bool]]) -> CubeState:
    cube = defaultdict(lambda : False)
    offset = len(seed)
    for y, row in enumerate(seed):
        for x, is_on in enumerate(row):
            if is_on:
                cube[(x - offset, y - offset, 0)] = True
    return cube

def get_adjacent(cell: Tuple[int, int, int]) -> List[Tuple[int, int, int]]:
    x, y, z = cell 
    adjacents = product([x, x+1, x-1], [y, y+1, y-1], [z, z+1, z-1])
    return list(filter(lambda x: x != cell, adjacents))

def simulate_cycle(cube: CubeState) -> CubeState:
    candidates = set(sum([get_adjacent(cell) for cell in cube] , []) + list(cube.keys()))
    new_cube = defaultdict(lambda : False)
    for cell in candidates:
        active_adjacents = sum(itemgetter(*get_adjacent(cell))(cube))
        if cube[cell] and active_adjacents in (2,3):
            new_cube[cell] = True
        if not cube[cell] and active_adjacents == 3:
            new_cube[cell] = True
    return new_cube

def simulate_six_cycles(plane: List[List[bool]]) -> int:
    cube = grid_to_cube(plane)
    for _ in range(6):
        cube = simulate_cycle(cube)
    return sum(cube.values())
    
HyperState = Dict[Tuple[int, int, int, int], bool]

def grid_to_hypercube(seed: List[List[bool]]) -> HyperState:
    hypercube = defaultdict(lambda : False)
    offset = len(seed)
    for y, row in enumerate(seed):
        for x, is_on in enumerate(row):
            if is_on:
                hypercube[(x - offset, y - offset, 0, 0)] = True
    return hypercube

def get_hyper_adjacent(cell: Tuple[int, int, int, int]) -> List[Tuple[int, int, int, int]]:
    x, y, z, w = cell 
    adjacents = product([x, x+1, x-1], [y, y+1, y-1], [z, z+1, z-1], [w, w+1, w-1])
    return list(filter(lambda x: x != cell, adjacents))

def simulate_hyper_cycle(hypercube: HyperState) -> HyperState:
    candidates = set(sum([get_hyper_adjacent(cell) for cell in hypercube] , []) + list(hypercube.keys()))
    new_hypercube = defaultdict(lambda : False)
    for cell in candidates:
        active_adjacents = sum(itemgetter(*get_hyper_adjacent(cell))(hypercube))
        if hypercube[cell] and active_adjacents in (2,3):
            new_hypercube[cell] = True
        if not hypercube[cell] and active_adjacents == 3:
            new_hypercube[cell] = True
    return new_hypercube

def simulate_six_hyper_cycles(plane: List[List[bool]]) -> int:
    hypercube = grid_to_hypercube(plane)
    for _ in range(6):
        hypercube = simulate_hyper_cycle(hypercube)
    return sum(hypercube.values())

if __name__ == "__main__":
    print("Part One: ", simulate_six_cycles(parse_as_grid(get_data())))
    print("Part Two: ", simulate_six_hyper_cycles(parse_as_grid(get_data())))