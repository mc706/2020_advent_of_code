module Main where

import Prelude
import Data.Set as Set
import Data.Function (applyN)
import Data.Maybe (Maybe(..))
import Data.String (split, Pattern(..), toCodePointArray, codePointFromChar)
import Data.Array (mapWithIndex, concatMap, concat, catMaybes, fromFoldable, filter, mapMaybe, length, elem)
import Data.List.Lazy (replicateM)
import Effect (Effect)
import Effect.Console (log)
import Node.FS.Sync (readTextFile)
import Node.Encoding (Encoding(..))

combinations :: Int -> Array Int -> Array (Array Int)
combinations i = replicateM i >>> map fromFoldable

type Plane
  = Array (Array Boolean)

parse :: String -> Plane
parse = map (map (_ == (codePointFromChar '#'))) <<< map toCodePointArray <<< split (Pattern "\n")

data CubeCell
  = CubeCell Int Int Int

instance eqCubeCell :: Eq CubeCell where
  eq (CubeCell x y z) (CubeCell a b c) = x == a && y == b && z == c

instance ordCubeCell :: Ord CubeCell where
  compare (CubeCell x y z) (CubeCell a b c) =
    let
      t1 = 10000 * x + 100 * y + z

      t2 = 10000 * a + 100 * b + c
    in
      if t1 > t2 then GT else if t1 < t2 then LT else EQ

type Cube
  = Set.Set CubeCell

cubeFromPlane :: Plane -> Cube
cubeFromPlane =
  mapWithIndex (\y row -> mapWithIndex (\x cell -> if cell then Just $ CubeCell x y 0 else Nothing) row)
    >>> concat
    >>> catMaybes
    >>> Set.fromFoldable

adjacentCubes :: CubeCell -> Array CubeCell
adjacentCubes (CubeCell x y z) =
  let
    move :: Array Int -> Maybe CubeCell
    move coords = case coords of
      [ dx, dy, dz ] -> Just $ CubeCell (x + dx) (y + dy) (z + dz)
      _ -> Nothing
  in
    combinations 3 [ -1, 0, 1 ]
      # filter (_ /= [ 0, 0, 0 ])
      # mapMaybe move

simulateCycle :: Cube -> Cube
simulateCycle cube =
  let
    isActive :: CubeCell -> Boolean
    isActive cell = Set.member cell cube

    countActive :: CubeCell -> Int
    countActive cell = length $ filter isActive $ adjacentCubes cell

    willBeActive :: CubeCell -> Boolean
    willBeActive cell =
      if isActive cell then
        elem (countActive cell) [ 2, 3 ]
      else
        3 == countActive cell
  in
    Set.filter willBeActive $ Set.fromFoldable $ concatMap adjacentCubes $ Set.toUnfoldable cube

partOne :: String -> Int
partOne =
  parse
    >>> cubeFromPlane
    >>> applyN simulateCycle 6
    >>> Set.size

data HyperCubeCell
  = HyperCubeCell Int Int Int Int

instance eqHyperCubeCell :: Eq HyperCubeCell where
  eq (HyperCubeCell x y z w) (HyperCubeCell a b c d) = x == a && y == b && z == c && w == d

instance ordHyperCubeCell :: Ord HyperCubeCell where
  compare (HyperCubeCell x y z w) (HyperCubeCell a b c d) =
    let
      t1 = 1000000 * a + 10000 * y + 100 * z + w

      t2 = 1000000 * a + 10000 * b + 100 * c + d
    in
      if t1 > t2 then GT else if t1 < t2 then LT else EQ

type HyperCube
  = Set.Set HyperCubeCell

hyperCubeFromPlane :: Plane -> HyperCube
hyperCubeFromPlane =
  mapWithIndex (\y row -> mapWithIndex (\x cell -> if cell then Just $ HyperCubeCell x y 0 0 else Nothing) row)
    >>> concat
    >>> catMaybes
    >>> Set.fromFoldable

adjacentHyperCubes :: HyperCubeCell -> Array HyperCubeCell
adjacentHyperCubes (HyperCubeCell x y z w) =
  let
    move :: Array Int -> Maybe HyperCubeCell
    move coords = case coords of
      [ dx, dy, dz, dw ] -> Just $ HyperCubeCell (x + dx) (y + dy) (z + dz) (w + dw)
      _ -> Nothing
  in
    combinations 4 [ -1, 0, 1 ]
      # filter (_ /= [ 0, 0, 0, 0 ])
      # mapMaybe move

simulateHyperCycle :: HyperCube -> HyperCube
simulateHyperCycle hyperCube =
  let
    isActive :: HyperCubeCell -> Boolean
    isActive = flip Set.member hyperCube

    countActive :: HyperCubeCell -> Int
    countActive = length <<< filter isActive <<< adjacentHyperCubes

    willBeActive :: HyperCubeCell -> Boolean
    willBeActive cell =
      if isActive cell then
        elem (countActive cell) [ 2, 3 ]
      else
        3 == countActive cell
  in
    Set.filter willBeActive $ Set.fromFoldable $ concatMap adjacentHyperCubes $ Set.toUnfoldable hyperCube

partTwo :: String -> Int
partTwo =
  parse
    >>> hyperCubeFromPlane
    >>> applyN simulateHyperCycle 6
    >>> Set.size

main :: Effect Unit
main = do
  contents <- readTextFile UTF8 "../2020-12-17.txt"
  log $ "Part One: " <> (show $ partOne contents)
  log $ "Part Two: " <> (show $ partTwo contents)
