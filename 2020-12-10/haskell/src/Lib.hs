module Lib
  ( partOne,
    partTwo,
  )
where

import Data.List

type Gaps = (Int, Int, Int)

ones :: Gaps -> Int
ones (x, _, _) = x

twos :: Gaps -> Int
twos (_, x, _) = x

threes :: Gaps -> Int
threes (_, _, x) = x

getGapsDistribution :: [Int] -> Gaps
getGapsDistribution nums =
  let withMax :: [Int]
      withMax =
        sort $ maximum nums + 3 : nums

      findGap :: (Int, Gaps) -> Int -> (Int, Gaps)
      findGap (prev, (a, b, c)) curr =
        case curr - prev of
          1 -> (curr, (a + 1, b, c))
          2 -> (curr, (a, b + 1, c))
          3 -> (curr, (a, b, c + 1))
          _ -> (curr, (a, b, c))
   in snd $ foldl findGap (0, (0, 0, 0)) withMax

onesByThrees :: [Int] -> Int
onesByThrees nums =
  product $ [ones, threes] <*> pure (getGapsDistribution nums)

partOne :: String -> IO ()
partOne filePath = do
  contents <- readFile filePath
  putStrLn $ "Part One: " <> show (onesByThrees $ map read $ lines contents)

truncateBelow :: [Int] -> Int -> [Int]
truncateBelow nums low =
  filter (>= low) nums

findArrangement :: [Int] -> Int
findArrangement [_] = 1
findArrangement (x : xs) =
  sum $ findArrangement . truncateBelow xs <$> filter (`elem` xs) (map (x +) [1 .. 3])

totalArrangements :: [Int] -> Int
totalArrangements nums =
  findArrangement $ sort $ 0 : maximum nums + 3 : nums

partTwo :: String -> IO ()
partTwo filePath = do
  contents <- readFile filePath
  putStrLn $ "Part Two: " <> show (totalArrangements $ map read $ lines contents)