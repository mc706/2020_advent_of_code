module Main where

import Lib

main :: IO ()
main = do
  partOne "../2020-12-10.txt"
  partTwo "../2020-12-10.txt"
