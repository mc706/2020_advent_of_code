module Main where

import Prelude
import Data.Array (foldl, sort)
import Effect (Effect)
import Effect.Console (log)
import Node.FS.Sync (readTextFile)
import Node.Encoding (Encoding(..))

type Gaps
  = { ones :: Int, twos :: Int, threes :: Int }

type GapAccumulator
  = { prev :: Int, gaps :: Gaps }

findGap :: GapAccumulator -> Int -> GapAccumulator
findGap { prev, gaps: gaps@{ ones, twos, threes } } curr = case curr - prev of
  1 -> { prev: curr, gaps: gaps { ones = ones + 1 } }
  2 -> { prev: curr, gaps: gaps { twos = twos + 1 } }
  3 -> { prev: curr, gaps: gaps { threes = threes + 1 } }
  _ -> { prev: curr, gaps: gaps }

getGapsDistribution :: Array Int -> Gaps
getGapsDistribution nums = _.gaps $ foldl findGap { prev: 0, gaps: { ones: 0, twos: 0, threes: 0 } } nums

addMax :: Array Int -> Array Int
addMax nums = maximum nums + 3 : nums

onesByThrees :: GapAccumulator -> Int
onesByThrees { ones, threes } = ones * threes

findOnesByThrees :: Array Int -> Int
findOnesByThrees nums =
  nums
    # addMax
    # sort
    # getGapsDistribution
    # onesByThrees

partOne :: String -> Int
partOne = const - 1

partTwo :: String -> Int
partTwo = const - 1

main :: Effect Unit
main = do
  contents <- readTextFile UTF8 "../2020-12-13.txt"
  log $ "Part One: " <> (show $ partOne contents)
  log $ "Part Two: " <> (show $ partTwo contents)
