from typing import List, Set
from enum import Enum
from dataclasses import dataclass, replace
from collections import Counter
from functools import reduce 

def get_data() -> List[str]:
    with open('../2020-12-24.txt', 'r') as input_file:
        return input_file.read().split('\n')

class Direction(Enum):
    East = "e"
    SouthEast = "se"
    NorthEast = "ne"
    West = "w"
    SouthWest = "sw"
    NorthWest = "nw"

@dataclass(frozen=True)
class Coord:
    x: float = 0
    y: float = 0

    def __add__(self, dir: Direction) -> 'Coord':
        if dir == Direction.East:
            return replace(self, x=self.x+1)
        if dir == Direction.West:
            return replace(self, x=self.x-1)
        if dir == Direction.NorthEast:
            return replace(self, y=self.y+1, x=self.x + .5)
        if dir == Direction.SouthWest:
            return replace(self, y=self.y-1, x=self.x-.5)
        if dir == Direction.NorthWest:
            return replace(self, y =self.y+1, x = self.x-.5)
        if dir == Direction.SouthEast:
            return replace(self, y =self.y-1, x = self.x+.5)

def parseRow(raw: str) -> List[Direction]:
    output = []
    while raw:
        for direction in Direction:
            if raw.startswith(direction.value):
                output.append(direction)
                raw = raw[len(direction.value):]
    return output

def parse(raw: List[str]) -> List[List[Direction]]:
    return list(map(parseRow, raw))

def black_tiles(dirs: List[List[Direction]]) -> Set[Coord]:
    def get_location(steps: List[Direction]) -> Coord:
        return sum(steps, Coord())
    position_counts = Counter(map(get_location, dirs))
    return {coord for coord, count in position_counts.items() if count % 2 != 0}

def adjacent(coord: Coord) -> Set[Coord]:
    return {
        Coord(coord.x-1, coord.y),
        Coord(coord.x+1, coord.y),
        Coord(coord.x+.5, coord.y+1),
        Coord(coord.x-.5, coord.y+1),
        Coord(coord.x+.5, coord.y-1),
        Coord(coord.x-.5, coord.y-1),
    }

def step(tiles: Set[Coord]) -> Set[Coord]:
    considered = set.union(*map(adjacent, tiles))
    next_tiles = set()
    for tile in considered:
        adjacent_blacks = adjacent(tile) & tiles
        if tile in tiles:
            if len(adjacent_blacks) in (1, 2):
                next_tiles.add(tile)
        else:
            if len(adjacent_blacks) == 2:
                next_tiles.add(tile)
    return next_tiles    

def game_of_life(tiles: Set[Coord]):
    for _ in range(100):
        tiles = step(tiles)
    return tiles

if __name__ == "__main__":
    print("Part One: ", len(black_tiles(parse(get_data()))))
    print('Part Two: ', len(game_of_life(black_tiles(parse(get_data())))))