module Main where

import Prelude
import Data.Maybe (Maybe)
import Data.Array (concatMap, length, filter, (!!), mapMaybe, mapWithIndex, singleton)
import Data.String (toCodePointArray, codePointAt, fromCodePointArray) as String
import Data.String.CodePoints (codePointFromChar, CodePoint, singleton) as String
import Effect (Effect)
import Effect.Console (log)

countFullSeats :: Array String -> Int
countFullSeats =
  concatMap String.toCodePointArray
    >>> filter ((==) (String.codePointFromChar '#'))
    >>> length

type Direction
  = { dx :: Int, dy :: Int }

makeDir :: Int -> Int -> Direction
makeDir x y = { dx: x, dy: y }

compass :: Array Direction
compass =
  [ makeDir (-1) (-1)
  , makeDir 0 (-1)
  , makeDir 1 (-1)
  , makeDir (-1) 0
  , makeDir 1 0
  , makeDir (-1) 1
  , makeDir (-1) 1
  , makeDir 0 1
  , makeDir 1 1
  ]

gridGet :: Array String -> Int -> Int -> Maybe String.CodePoint
gridGet grid x y = grid !! y >>= String.codePointAt x

getAdjacents :: Array String -> Int -> Int -> Array String.CodePoint
getAdjacents grid x y = mapMaybe (\{ dx, dy } -> gridGet grid (x + dx) (y + dy)) compass

decideSeat :: Array String -> Int -> Int -> String.CodePoint -> String.CodePoint
decideSeat grid y x seat = case String.singleton seat of
  "#" ->
    if countFullSeats $ singleton $ String.fromCodePointArray $ getAdjacents grid x y >= 4 then
      String.codePointFromChar 'L'
    else
      seat
  "L" ->
    if countFullSeats $ singleton $ String.fromCodePointArray $ getAdjacents grid x y == 0 then
      String.codePointFromChar '#'
    else
      seat
  _ -> seat

stepSeats :: Array String -> Array String
stepSeats grid =
  mapWithIndex
    ( \y row ->
        mapWithIndex (decideSeat grid y) (String.toCodePointArray row) # String.fromCodePointArray
    )
    grid

main :: Effect Unit
main = do
  log "🍝"
