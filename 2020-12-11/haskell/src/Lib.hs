module Lib
  ( partOne,
    partTwo,
  )
where

import Data.Maybe

countFullSeats :: [String] -> Int
countFullSeats =
  length . filter (== '#') . concat

get :: Int -> [a] -> Maybe a
get _ [] = Nothing
get 0 (x : _) = Just x
get i (_ : xs) = get (i - 1) xs

compass = [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]

gridGet :: [String] -> Int -> Int -> Maybe Char
gridGet grid x y =
  get y grid >>= get x

getAdjacents :: [String] -> Int -> Int -> [Char]
getAdjacents grid x y =
  mapMaybe (\(dx, dy) -> gridGet grid (x + dx) (y + dy)) compass

mapIdx :: (a -> Int -> b) -> [a] -> [b]
mapIdx f xs = zipWith f xs [0 ..]

decideSeat :: [String] -> Int -> Char -> Int -> Char
decideSeat grid y seat x =
  case seat of
    '#' ->
      if countFullSeats (pure $ getAdjacents grid x y) >= 4
        then 'L'
        else seat
    'L' ->
      if countFullSeats (pure $ getAdjacents grid x y) == 0
        then '#'
        else seat
    _ -> seat

stepSeats :: [String] -> [String]
stepSeats grid =
  mapIdx
    ( \row y ->
        mapIdx (decideSeat grid y) row
    )
    grid

converge :: Eq a => (a -> a) -> a -> a
converge = until =<< ((==) =<<)

countConvergedSeats :: ([String] -> [String]) -> [String] -> Int
countConvergedSeats fn grid =
  countFullSeats $ converge fn grid

partOne :: String -> IO ()
partOne filePath = do
  contents <- readFile filePath
  putStrLn $ ("Part One: " ++) $ show $ countConvergedSeats stepSeats $ lines contents

getLineFrom :: [String] -> Int -> Int -> (Int, Int) -> [Char]
getLineFrom grid x y (dx, dy) =
  case gridGet grid (x + dx) (y + dy) of
    Just '.' -> '.' : getLineFrom grid (x + dx) (y + dy) (dx, dy)
    Just o -> pure o
    Nothing -> []

getLos :: [String] -> Int -> Int -> [Char]
getLos grid x y =
  concatMap (getLineFrom grid x y) compass

decideSeatLos :: [String] -> Int -> Char -> Int -> Char
decideSeatLos grid y seat x =
  case seat of
    '#' ->
      if countFullSeats (pure $ getLos grid x y) >= 5
        then 'L'
        else seat
    'L' ->
      if countFullSeats (pure $ getLos grid x y) == 0
        then '#'
        else seat
    _ ->
      seat

stepSeatsLos :: [String] -> [String]
stepSeatsLos grid =
  mapIdx
    ( \row y ->
        mapIdx (decideSeatLos grid y) row
    )
    grid

partTwo :: String -> IO ()
partTwo filePath = do
  contents <- readFile filePath
  putStrLn $ ("Part Two: " ++) $ show $ countConvergedSeats stepSeatsLos $ lines contents
