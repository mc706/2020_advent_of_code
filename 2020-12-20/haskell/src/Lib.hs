{-# LANGUAGE TupleSections #-}

module Lib where

import qualified Data.Char as Char
import Data.List (transpose)
import Data.List.Split (splitOn)
import qualified Data.Map.Strict as Map

data Tile = Tile
  { number :: Int,
    image :: [String]
  }
  deriving (Show, Ord, Eq)

data Rotation
  = None
  | R1
  | R2
  | R3

data Orientation = Rotation Bool

top :: Tile -> String
top Tile {image = image} = head image

bottom :: Tile -> String
bottom Tile {image = image} = last image

left :: Tile -> String
left Tile {image = image} = head <$> image

right :: Tile -> String
right Tile {image = image} = last <$> image

rotateTile :: Tile -> Tile
rotateTile Tile {number = number, image = image} = Tile {number = number, image = transpose image}

flipTile :: Tile -> Tile
flipTile Tile {number = number, image = image} = Tile {number = number, image = reverse <$> image}

type Grid = Map.Map (Int, Int) (Tile, Orientation)

findGrid' :: Int -> [Tile] -> Grid -> Grid
findGrid' size tiles grid = undefined

findGrid :: [Tile] -> Grid
findGrid tiles =
  let size = foor $ sqrt $ fromIntegral $ length tiles
   in findGrid' size tiles Map.empty

parseTile :: [String] -> Tile
parseTile (header : image_data) =
  let tileNum = read $ filter Char.isNumber header
   in Tile tileNum image_data

parse :: String -> [Tile]
parse raw =
  parseTile . splitOn "\n" <$> splitOn "\n\n" raw

partOne :: String -> IO ()
partOne filePath = do
  contents <- readFile filePath
  putStrLn $ "Part One: " ++ show (map number $findCorners $ sideMap $ parse contents)

-- [1753, 2843, 3083, 1489]

partTwo :: String -> IO ()
partTwo filePath = do
  contents <- readFile filePath
  putStrLn $ "Part Two: " ++ show (-1)
