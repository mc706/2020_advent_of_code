from typing import List, Tuple, Dict, Optional
from dataclasses import dataclass
from functools import cached_property, reduce, partial, lru_cache
from collections import defaultdict 
from operator import mul 
from enum import Enum, auto

product = partial(reduce, mul)

def get_data() -> List[str]:
    with open('../2020-12-20.txt', 'r') as input_file:
        return input_file.read().split('\n\n')

@dataclass(frozen=True)
class Tile:
    number: int
    data: Tuple[str]

    @classmethod
    def parse(cls, raw: str) -> 'Tile':
        title, *data = raw.split('\n')
        num = int("".join(filter(lambda x: x.isnumeric(), title)))
        return cls(num, tuple(data))

    @cached_property
    def sides(self) -> Tuple[str]:
        top = self.data[0]
        bottom = self.data[-1]
        left = "".join(map(lambda xs: xs[0], self.data))
        right = "".join(map(lambda xs: xs[-1], self.data))
        return (top, right, bottom, left, top[::-1], right[::-1], bottom[::-1], left[::-1])

class Orientation(Enum):
    none = auto()
    r1  = auto()
    r2 = auto() 
    r3 = auto()
    f = auto()
    fr1 = auto()
    fr2 = auto()
    fr3 = auto()

def rotate(matrix: List[str]) -> List[str]:
    """
    >>> rotate(["ab", "cd"])
    ['ca', 'db']
    """
    return list(map(lambda x: "".join(x), zip(*matrix[::-1])))

def identity(a):
    return a

def compose(*functions):
    def compose2(f, g):
        return lambda x: f(g(x))
    return reduce(compose2, functions, identity)

def flip(matrix: List[str]) -> List[str]:
    """
    >>> flip(["ab", "cd"])
    ['cd', 'ab']
    """
    return matrix[::-1]

@lru_cache(maxsize=None)
def orient(orientation: Orientation, tile: Tile) -> Tuple[str, str, str, str]:
    """
    >>> orient(Orientation.none, Tile(1, ("ab", "cd")))
    ('ab', 'bd', 'cd', 'ac')
    >>> orient(Orientation.r1, Tile(1, ("ab", "cd")))
    ('ca', 'ab', 'db', 'cd')
    >>> orient(Orientation.r2, Tile(1, ("ab", "cd")))
    ('dc', 'ca', 'ba', 'db')
    """
    transformations = {
        Orientation.none: identity,
        Orientation.r1: rotate,
        Orientation.r2: compose(rotate, rotate),
        Orientation.r3: compose(rotate, rotate, rotate),
        Orientation.f: flip,
        Orientation.fr1: compose(rotate, flip),
        Orientation.fr2: compose(rotate, rotate, flip),
        Orientation.fr3: compose(rotate, rotate, rotate, flip),
    }
    data = transformations[orientation](tile.data)
    top = data[0]
    bottom = data[-1]
    left = "".join(map(lambda xs: xs[0], data))
    right = "".join(map(lambda xs: xs[-1], data))
    return (top, right, bottom , left)

def parse(raw: List[str]) -> List[Tile]:
    return list(map(Tile.parse, raw))

def get_corners(tiles: List[Tile]) -> List[Tile]:
    sides = defaultdict(list)
    for tile in tiles:
        for side in tile.sides:
            sides[side].append(tile.number)
    side_counts = defaultdict(lambda: 0)
    for tile in tiles:
        side_counts[tile.number] = sum([1 for ts in sides.values() if tile.number in ts and len(ts) == 1])
    corner_tile_numbers = [tile_num for tile_num, missing_sides in side_counts.items() if missing_sides == 4]
    print(corner_tile_numbers)
    return corner_tile_numbers

Grid = Tuple[Tuple[Optional[Tuple[Orientation, Tile]]]]

def find_first_empty(grid: Grid) -> Optional[Tuple[int, int]]:
    for y, row in enumerate(grid):
        for x, cell in enumerate(row):
            if cell is None:
                return (x, y)
    return None 

def update_grid(grid: Grid, loc: Tuple[int, int], value: Tuple[Orientation, Tile]) -> Grid:
    """
    >>> update_grid((("a", "b"),("c", "d")), (1,1), 'e')
    (('a', 'b'), ('c', 'e'))
    """
    def tup_replace(tup, ix, val):
        return tup[:ix] + (val,) + tup[ix+1:]
    x, y = loc    
    return tup_replace(grid, y, tup_replace(grid[y], x, value))

def arrange_tiles(tiles: List[Tile]) -> Tuple[Tuple[Tuple[Orientation, Tile]]]:
    tileset = set(tiles)
    grid_size = int(len(tiles)**.5)
    start_tile_id, *other_corners = get_corners(tiles)
    from_id = lambda tile_id: next(filter(lambda x: x.number == tile_id, tiles))

    def find_grid(grid: Grid, tile: Tile, orientation: Orientation):
        first_empty = find_first_empty(grid)
        # print(first_empty)
        grid1 = update_grid(grid, first_empty, (orientation, tile))
        if find_first_empty(grid1) is None: return grid1
        used_tiles = {cell[1] for row in grid1 for cell in row if cell is not None}
        available_tiles = tileset - used_tiles
        x, y = first_empty
        print(first_empty, len(available_tiles))
        for n_tile in available_tiles:
            for n_orientation in list(Orientation):
                top, _, _, left = orient(n_orientation, n_tile)
                print(n_tile.number, n_tile, top, left)
                if y > 0:
                    north_border = grid1[y-1][x]
                    _, _, bot, _ = orient(*north_border)
                    if top != bot:
                        continue
                if x > 0:
                    west_border = grid1[y][x-1]
                    _, right, _, _ = orient(*west_border)
                    if left != right:
                        continue
                if x > 0 and y > 0:
                    print("Im here")
                final_grid_check = find_grid(grid1, n_tile, n_orientation)
                if final_grid_check is not None:
                    return final_grid_check
        
    empty = tuple(tuple(None for _ in range(grid_size)) for _ in range(grid_size))

    grid = next(map(partial(find_grid, empty, from_id(start_tile_id)), list(Orientation)))

    print(grid)
    return grid

def image_from_tiles(grid: Tuple[Tuple[Orientation, Tile]]) -> Tuple[str]:
    """
    concatenate tiles to single image
    """
    return []

def apply_sea_monsters(image: Tuple[str]) -> Tuple[str]:
    coord_grid = {}
    for y, row in enumerate(image):
        for x, char in enumerate(row):
            coord_grid[(x, y)] = char
    sea_monster = """                  # 
#    ##    ##    ###
 #  #  #  #  #  #   """
    monster_grid = {}
    for y, row, in enumerate(sea_monster.split('\n')):
        for x, char in enumerate(row):
            if char == "#":
                monster_grid[(x,y)] = char
    # unwind grid
    return ('','')

def get_seas_roughness(image: List[str]) -> int:
    return len(list(filter(lambda x: x == "#", "".join(image))))

if __name__ == "__main__":
    import doctest
    doctest.testmod()
    print("Part One: ", product(get_corners(parse(get_data()))))
    # print("Part Two: ", get_seas_roughness(apply_sea_monsters(image_from_tiles(arrange_tiles(parse(get_data()))))))