module Main where

import Lib

main :: IO ()
main = do
  partOne "../2020-12-14.txt"
  partTwo "../2020-12-14.txt"
