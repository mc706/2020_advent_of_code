{-# LANGUAGE TupleSections #-}

module Lib
  ( partOne,
    partTwo,
  )
where

import qualified Data.Char as Char
import Data.List (foldl')
import Data.Map (Map)
import qualified Data.Map as Map
import Numeric (showIntAtBase)

type Binary36 = String

type BinaryMask = String

data Command
  = Mask BinaryMask
  | Mem Int Int

emptyMask :: BinaryMask
emptyMask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

zeroMask :: BinaryMask
zeroMask = "000000000000000000000000000000000000"

parse :: String -> Command
parse raw =
  case raw of
    ('m' : 'a' : 's' : 'k' : _) -> Mask $ words raw !! 2
    ('m' : 'e' : 'm' : _) ->
      case words raw of
        [first, _, val] ->
          Mem (read $ filter Char.isDigit first) (read val)

lpad :: Int -> String -> String
lpad l str = replicate (l - length str) '0' ++ take l str

intTo36Bin :: Int -> Binary36
intTo36Bin num = lpad 36 $ showIntAtBase 2 Char.intToDigit num ""

intFromBin :: Binary36 -> Int
intFromBin = foldl' (\acc x -> acc * 2 + Char.digitToInt x) 0

applyMask :: BinaryMask -> Int -> Int
applyMask mask value =
  intFromBin $ zipWith (\m v -> if m == 'X' then v else m) mask (intTo36Bin value)

runCommandV1 :: (BinaryMask, Map Int Int) -> Command -> (BinaryMask, Map Int Int)
runCommandV1 (mask, register) command =
  case command of
    Mask newMask -> (newMask, register)
    Mem slot val -> (mask, Map.insert slot (applyMask mask val) register)

versionOne :: [Command] -> Map Int Int
versionOne commands =
  snd $ foldl runCommandV1 (emptyMask, Map.empty) commands

totalMemory :: Map Int Int -> Int
totalMemory = Map.foldr (+) 0

partOne :: String -> IO ()
partOne filePath = do
  contents <- readFile filePath
  putStrLn $ "Part One: " <> show (totalMemory $ versionOne $ map parse $ lines contents)

maskAddr :: BinaryMask -> Int -> BinaryMask
maskAddr mask value =
  let applyMask' :: Char -> Char -> Char
      applyMask' m v =
        case m of
          'X' -> 'X'
          '0' -> v
          '1' -> '1'
   in zipWith applyMask' mask (intTo36Bin value)

addressMaskToAddresses :: BinaryMask -> [Int]
addressMaskToAddresses maskedAddress =
  let reducer :: [String] -> Char -> [String]
      reducer perms c =
        case c of
          'X' -> concatMap (<$> perms) [(++ "0"), (++ "1")]
          _ -> map (++ [c]) perms
   in intFromBin <$> foldl reducer [""] maskedAddress

getMaskAddrs :: BinaryMask -> Int -> [Int]
getMaskAddrs mask value =
  addressMaskToAddresses $ maskAddr mask value

runCommandV2 :: (BinaryMask, Map Int Int) -> Command -> (BinaryMask, Map Int Int)
runCommandV2 (mask, register) command =
  case command of
    Mask newMask -> (newMask, register)
    Mem slot val -> (mask, Map.union (Map.fromList $ map (,val) $ getMaskAddrs mask slot) register)

versionTwo :: [Command] -> Map Int Int
versionTwo commands =
  snd $ foldl runCommandV2 (zeroMask, Map.empty) commands

partTwo :: String -> IO ()
partTwo filePath = do
  contents <- readFile filePath
  putStrLn $ "Part Two: " <> show (totalMemory $ versionTwo $ map parse $ lines contents)