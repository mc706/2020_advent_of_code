module Main where

import Prelude
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Map (Map, values, empty, insert)
import Data.Int53 (Int53(..), toInt53)
import Data.String (split, Pattern(..), toCodePointArray, fromCodePointArray, take, trim, dropWhile, replace, Replacement(..), drop)
import Data.String.CodePoints (CodePoint, codePointFromChar)
import Data.Int (toStringAs, binary, fromStringAs, fromString)
import Data.Array (foldl, mapMaybe, zipWith, foldr)
import Text.Format (format, zeroFill, width)
import Effect (Effect)
import Effect.Console (log)
import Node.FS.Sync (readTextFile)
import Node.Encoding (Encoding(..))

type BinaryMask
  = String

type BinaryNum
  = String

data Command
  = Mask BinaryMask
  | Mem Int Int

instance showCommand :: Show Command where
  show c = case c of
    Mask m -> "Mask " <> m
    Mem k v -> "Mem " <> show k <> " " <> show v

charX :: CodePoint
charX = codePointFromChar 'X'

charEq :: CodePoint
charEq = codePointFromChar '='

char1 :: CodePoint
char1 = codePointFromChar '1'

char0 :: CodePoint
char0 = codePointFromChar '0'

parse :: String -> Maybe Command
parse raw = case take 3 raw of
  "mas" -> Just $ Mask $ drop 2 $ dropWhile ((/=) charEq) raw
  "mem" -> case split (Pattern " = ") raw of
    [ cmd, val ] -> Mem <$> (fromString $ replace (Pattern "mem[") (Replacement "") $ replace (Pattern "]") (Replacement "") cmd) <*> (fromString val)
    _ -> Nothing
  _ -> Nothing

splitOnNewlines :: String -> Array String
splitOnNewlines = split (Pattern "\n")

type ExecutionStep
  = { mask :: BinaryMask
    , register :: Map Int Int53
    }

intToBinary :: Int -> BinaryNum
intToBinary val = format (zeroFill <> width 36) $ toStringAs binary val

binToInt :: BinaryNum -> Int53
binToInt num =
  let
    convert :: CodePoint -> Int53
    convert c = if c == char1 then toInt53 1 else toInt53 0
  in
    foldr (\c a -> (convert c) + (a * (toInt53 2))) (toInt53 0) (toCodePointArray num)

applyMask :: BinaryMask -> Int -> Int53
applyMask mask value =
  value
    # intToBinary
    # toCodePointArray
    # zipWith (\m v -> if m == charX then v else m) (toCodePointArray mask)
    # fromCodePointArray
    # binToInt

runCommandV1 :: ExecutionStep -> Command -> ExecutionStep
runCommandV1 step@{ mask, register } command = case command of
  Mask newMask -> step { mask = newMask }
  Mem slot val -> step { register = insert slot (applyMask mask val) register }

versionOne :: Array Command -> Map Int Int53
versionOne =
  foldl runCommandV1 { mask: "", register: empty }
    >>> _.register

totalMemory :: Map Int Int53 -> Int53
totalMemory = values >>> foldl (+) (toInt53 0)

partOne :: String -> Int53
partOne =
  splitOnNewlines
    >>> mapMaybe parse
    >>> versionOne
    >>> totalMemory

partTwo :: String -> Int
partTwo = const (-1)

main :: Effect Unit
main = do
  contents <- readTextFile UTF8 "../2020-12-14.txt"
  log $ "Part One: " <> (show $ partOne contents)
  log $ "Part Two: " <> (show $ partTwo contents)
