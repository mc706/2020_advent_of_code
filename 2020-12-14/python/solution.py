from typing import List, Union, Tuple, Dict
from enum import Enum
from dataclasses import dataclass

def get_data() -> List[str]:
    with open("../2020-12-14.txt", 'r') as input_file:
        return list(input_file.readlines())

class Instruction(Enum):
    MASK = 'mask'
    MEM = 'mem'

Command = Tuple[Instruction, Union[str, Tuple[int, int]]]

def parse_line(line: str) -> Command:
    inst, val = line.strip().split(' = ')
    if inst == "mask":
        return (Instruction.MASK, val)
    register = int(inst.replace("mem[", '').replace(']', ''))
    return (Instruction.MEM, (register, int(val)))

def parse(data: List[str]) -> List[Command]:
    return list(map(parse_line, data))
 
def apply_mask(mask: str, value: int) -> int:
    """
    >>> apply_mask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X", 0)
    64
    >>> apply_mask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X", 11)
    73
    >>> apply_mask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X", 101)
    101
    """
    val_bin = format(value, f"0{len(mask)}b")
    return int("".join(v if m == "X" else m for m, v in zip(mask, val_bin)), 2)

def replace_index(string: str, i: int, value: str)  -> str:
    """
    >>> replace_index("abc", 1, 'd')
    'adc'
    """
    return string[:i] + value + string[i+1:]

def apply_decoder_mask(mask: str, value: int) -> List[int]:
    """
    >>> apply_decoder_mask("000000000000000000000000000000X1001X", 42)
    [26, 27, 58, 59]
    """
    val_bin = format(value, f"0{len(mask)}b")
    output = []
    for m, v in zip(mask, val_bin):
        if m == 'X':
            output.append('X')
        elif m == "1":
            output.append('1')
        else: 
            output.append(v)
    mask_result = "".join(output)
    encoding_queue = [mask_result]
    final_addresses = []
    while encoding_queue:
        pattern = encoding_queue.pop(0)
        if 'X' not in pattern:
            final_addresses.append(pattern)
        else:
            first_index = pattern.find('X')
            encoding_queue.append(replace_index(pattern, first_index, '0'))
            encoding_queue.append(replace_index(pattern, first_index, '1'))
    return [int(address, 2) for address in final_addresses]

def final_decoder_memory(commands: List[Command]) -> Dict[int, int]:
    register = {}
    current_mask = "X" * 36
    for command in commands:
        cmd, args = command
        if cmd == Instruction.MASK:
            current_mask = args
        elif cmd == Instruction.MEM:
            slot, val = args
            slots = apply_decoder_mask(current_mask, slot)
            for encoded_slot in slots:
                register[encoded_slot] = val
    return register 

def final_register(commands: List[Command]) -> Dict[int,int]:
    register = {}
    current_mask = "X" * 36
    for command in commands:
        cmd, args = command
        if cmd == Instruction.MASK:
            current_mask = args
        elif cmd == Instruction.MEM:
            slot, val = args 
            register[slot] = apply_mask(current_mask, val)
    return register 

def total_register(register: Dict[int, int]) -> int:
    return sum(register.values())

def part_one() -> int:
    return total_register(final_register(parse(get_data())))

def part_two() -> int:
    return total_register(final_decoder_memory(parse(get_data())))

if __name__ == "__main__":
    import doctest
    doctest.testmod()
    print("Part One", part_one())
    print("Part Two", part_two())