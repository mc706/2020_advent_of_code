from typing import List, Set, Tuple, Dict

def get_data() -> List[str]:
    with open('../2020-12-21.txt', 'r') as input_file:
        return input_file.read().split('\n')

def parse_row(raw: str) -> Tuple[Set[str], Set[str]]:
    incredients, allergens = raw.split(' (contains ')
    return set(incredients.split(' ')), set(allergens.replace(")", '').split(', '))

def parse(raw: List[str]) -> List[Tuple[Set[str], Set[str]]]:
    return list(map(parse_row, raw))

def find_allergens(ingredients_allergens: List[Tuple[Set[str], Set[str]]]) -> Dict[str, str]:
    potentials_by_allergen = {}
    for ingredients, allergens in ingredients_allergens:
        for allergen in allergens:
            potentials_by_allergen.setdefault(allergen, []).append(ingredients.copy())
    found_allergens = {}
    while potentials_by_allergen:
        to_remove = []
        for allergen, foods in potentials_by_allergen.items():
            cross = set.intersection(*foods)
            if len(cross) == 1:
                food_with_allergen = cross.pop()
                found_allergens[allergen] = food_with_allergen
                for inner_foods in potentials_by_allergen.values():
                    for ingredient_set in inner_foods:
                        ingredient_set.discard(food_with_allergen)
                to_remove.append(allergen)
                break
        for allergen in to_remove:
            del potentials_by_allergen[allergen]
    return found_allergens

def sample() -> int:
    raw = """mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)""".split('\n')
    rows = parse(raw)
    allergens = find_allergens(rows)
    all_ingredients = sum([list(ingredients) for ingredients, _ in rows], [])
    return len([ingredient for ingredient in all_ingredients if ingredient not in allergens.values()])

def part_one() -> int:
    rows = parse(get_data())
    allergens = find_allergens(rows)
    all_ingredients = sum([list(ingredients) for ingredients, _ in rows], [])
    return len([ingredient for ingredient in all_ingredients if ingredient not in allergens.values()])

def part_two() -> str:
    allergens = find_allergens(parse(get_data()))
    output = []
    for key in sorted(allergens.keys()):
        output.append(allergens[key])
    return ",".join(output)

if __name__ == "__main__":
    # print("Sample Data: ", sample())
    print("Part One: ", part_one())
    print("Part Two: ", part_two())