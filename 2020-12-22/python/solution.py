from typing import List, Tuple

def get_data() -> Tuple[Tuple[int], Tuple[int]]:
    with open('../2020-12-22.txt', 'r') as input_file:
        data = input_file.read()
    player1, player2 = data.split('\n\n')
    player1_cleaned = tuple(map(int, player1.split('\n')[1:]))
    player2_cleaned = tuple(map(int, player2.split('\n')[1:]))
    return player1_cleaned, player2_cleaned

def play_game(player1: Tuple[int], player2: Tuple[int]) -> Tuple[int]:
    if not player1 or not player2:
        return player1 or player2
    p1card, *p1deck = player1 
    p2card, *p2deck = player2 
    if p1card > p2card:
        return play_game(tuple(p1deck + [p1card, p2card]), tuple(p2deck))
    else:
        return play_game(tuple(p1deck), tuple(p2deck + [p2card, p1card]))

def play_recusrive_game(player1: Tuple[int], player2: Tuple[int], game: int = 1) -> Tuple[bool, Tuple[int]]:
    mem = set()
    while player1 and player2:
        if (player1, player2, game) in mem:
            return (True, player1)
        mem.add((player1, player2, game))
        p1card, *p1deck = player1 
        p2card, *p2deck = player2
        if p1card <= len(p1deck) and p2card <= len(p2deck):
            player_one_win, _ = play_recusrive_game(tuple(p1deck[:p1card]), tuple(p2deck[:p2card]), game=game + 1)
        else: 
            player_one_win = p1card > p2card
        if player_one_win:
            player1 = tuple(p1deck + [p1card, p2card])
            player2 = tuple(p2deck)
        else:
            player1 = tuple(p1deck)
            player2 = tuple(p2deck + [p2card, p1card])
    return (True, player1) if player1 else (False, player2)

def score_final(final_deck: Tuple[int]) -> int:
    total = 0
    for i, num in enumerate(reversed(final_deck)):
        total += (i+1) * num 
    return total

def part_one() -> int:
    return score_final(play_game(*get_data()))

def part_two() -> int:
    _, final = play_recusrive_game(*get_data())
    return score_final(final)

if __name__ == "__main__":
    print("Part One: ", part_one())
    print("Part Two: ", part_two())