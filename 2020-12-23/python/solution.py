from typing import List 
from collections import deque 
from tqdm import tqdm

def crab_game(seed: int) -> int:
    cycle = deque(map(int, str(seed)))
    buffer = []
    for _ in range(100):
        print('cups', cycle)
        current = cycle[0]
        cycle.rotate(-1)
        buffer.append(cycle.popleft())
        buffer.append(cycle.popleft())
        buffer.append(cycle.popleft())
        cycle.rotate(1)
        for i in range(1, len(cycle)):
            if current - i in cycle:
                insert_point = cycle.index(current - i) + 1
                cycle.insert(insert_point, buffer.pop())
                cycle.insert(insert_point, buffer.pop())
                cycle.insert(insert_point, buffer.pop())
                break
            if current - i < min(cycle):
                insert_point = cycle.index(max(cycle)) + 1
                cycle.insert(insert_point, buffer.pop())
                cycle.insert(insert_point, buffer.pop())
                cycle.insert(insert_point, buffer.pop())
                break
        cycle.rotate(-1)
    while cycle[0] != 1:
        cycle.rotate()
    return int("".join(map(str,cycle)).replace('1', ''))
    

def crab_game_2(seed: int) -> int:
    cycle = list(map(int, str(seed)))
    highest = max(cycle)
    for i in range(highest + 1, 1_000_001):
        cycle.append(i)
    buffer = list()
    for _ in tqdm(range(10_000_000)):
        current = cycle[0]
        cycle.rotate(-1)
        buffer.append(cycle.popleft())
        buffer.append(cycle.popleft())
        buffer.append(cycle.popleft())
        cycle.rotate(1)
        target = current - 1
        while target in buffer:
            target -= 1
            if target < 1: 
                target = cycle.index(1_000_000)
                break
        insert_point = cycle.index(target) + 1
        cycle.rotate(insert_point)
        cycle.append(buffer.pop())
        cycle.append(buffer.pop())
        cycle.append(buffer.pop())
        cycle.rotate(-insert_point)
        cycle.rotate(-1)
    while cycle[0] != 1:
        cycle.rotate()
    one = cycle.pop()
    first = cycle.pop()
    second = cycle.pop()
    return first * second


if __name__ == "__main__":
    print("Part One: ", crab_game(974618352))
    print("Part Two: ", crab_game_2(974618352))
