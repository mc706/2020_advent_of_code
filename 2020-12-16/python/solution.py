from typing import List, Tuple, TypeVar, Iterable, Set, Optional
from dataclasses import dataclass
from functools import reduce, partial
from operator import mul 


def get_data() -> Tuple[List[str], List[int], List[List[int]]]:
    with open('../2020-12-16.txt', 'r') as input_file:
        data = input_file.read()
    rules, ticket, nearby = data.split('\n\n')
    rules = rules.split('\n')
    ticket = list(map(int, ticket.replace("your ticket:\n", "").split(',')))
    nearby = [list(map(int, line.split(','))) for line in nearby.replace("nearby tickets:\n", '').split('\n')]
    return rules, ticket, nearby

@dataclass
class Rule: 
    field: str 
    range1: Tuple[int, int]
    range2: Tuple[int, int]

    @classmethod
    def parse(cls, value: str) -> 'Rule':
        field, data = value.split(': ')
        range1s, range2s = data.split(' or ')
        range1l, range1u = range1s.split('-')
        range2l, range2u = range2s.split('-')
        return cls(field, (int(range1l), int(range1u)), (int(range2l), int(range2u)))

    def is_valid(self, value: int) -> bool:
        return is_bounded_by(value, self.range1) or is_bounded_by(value, self.range2)

def find_error_rate(rules: List[str], ticket: List[int], nearby:  List[List[int]]) -> int:
    parsed_rules = [Rule.parse(rule) for rule in rules]
    flattened_values = sum(nearby, [])
    invalid_values = [value for value in flattened_values if not any(rule.is_valid(value) for rule in parsed_rules)]
    return sum(invalid_values)

def remove_invalid_tickets(rules: List[Rule], tickets: List[List[int]]) ->  List[List[int]]:
    return list(filter(lambda ls: all(any(rule.is_valid(value) for rule in rules) for value in ls), tickets))

def determine_column_order(rules: List[Rule], tickets: List[List[int]]) -> List[str]:
    transposed = transpose(tickets)
    column_candidates = []
    for i, column in enumerate(transposed):
        column_candidates.append(set())
        for rule in rules:
            if all(rule.is_valid(value) for value in column):
                column_candidates[i].add(rule.field)
    while not all(map(set_singleton, column_candidates)):
        for column, others in exclusive(column_candidates):
            if field := set_singleton(column):
                for other in others:
                    other.discard(field)
    return sum(map(list, column_candidates), [])

def get_columns_from_nearby(rules: List[str], nearby:  List[List[int]]) -> List[str]:
    parsed_rules = [Rule.parse(rule) for rule in rules]
    valid_tickets = remove_invalid_tickets(parsed_rules, nearby)
    return determine_column_order(parsed_rules, valid_tickets)

def find_my_ticket_values(rules: List[str], ticket: List[int], nearby: List[List[int]]) -> int:
    columns = get_columns_from_nearby(rules, nearby)
    my_ticket = dict(zip(columns, ticket))
    print(my_ticket)
    product = partial(reduce, mul)
    return product(value for field, value in my_ticket.items() if field.startswith('departure'))

## Helper functions
A = TypeVar("A")

def transpose(matrix: List[List[A]]) -> List[List[A]]:
    return list(map(list, zip(*matrix)))

def exclusive(collection: List[A]) -> Iterable[Tuple[A, List[A]]]:
    for i, thing in enumerate(collection):
        yield thing, [other for j, other in enumerate(collection) if i != j]

def set_singleton(dataset: Set[A]) -> Optional[A]:
    return next(iter(dataset)) if len(dataset) == 1 else None

def is_bounded_by(value: int, bounds: Tuple[int, int]) -> bool:
    return bounds[0] <= value <= bounds[1]


if __name__ == "__main__":
    print("Part One: ", find_error_rate(*get_data()))
    print("Part Two: ", find_my_ticket_values(*get_data()))