module Main where

import Lib (partOne, partTwo)

main :: IO ()
main = do
  partOne "../2020-12-16.txt"
  partTwo "../2020-12-16.txt"