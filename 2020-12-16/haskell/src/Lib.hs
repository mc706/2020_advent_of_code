module Lib
  ( partOne,
    partTwo,
  )
where

import Data.List (isPrefixOf, transpose)
import Data.List.Split (splitOn)
import Data.Maybe (mapMaybe)

data Rule = Rule
  { field :: String,
    range1 :: (Int, Int),
    range2 :: (Int, Int)
  }
  deriving (Show, Eq)

parseRange :: String -> Maybe (Int, Int)
parseRange raw =
  case splitOn "-" raw of
    [l, r] -> Just (read l, read r)
    _ -> Nothing

parseRule :: String -> Maybe Rule
parseRule ruleRaw =
  case splitOn ": " ruleRaw of
    [field, rest] ->
      case splitOn " or " rest of
        [range1, range2] -> case (parseRange range1, parseRange range2) of
          (Just r1, Just r2) -> Just $ Rule {field = field, range1 = r1, range2 = r2}
          _ -> Nothing
        _ -> Nothing
    _ -> Nothing

parseRules :: String -> [Rule]
parseRules = mapMaybe parseRule . lines

parseTicket :: String -> [Int]
parseTicket ticket = read <$> splitOn "," ticket

parseMyTicket :: String -> [Int]
parseMyTicket raw =
  case lines raw of
    [_, ticket] -> parseTicket ticket
    _ -> []

parseNearby :: String -> [[Int]]
parseNearby raw =
  case lines raw of
    (_ : rest) -> parseTicket <$> rest
    _ -> []

parse :: String -> ([Rule], [Int], [[Int]])
parse raw =
  case splitOn "\n\n" raw of
    [rules, ticket, nearby] -> (parseRules rules, parseMyTicket ticket, parseNearby nearby)
    _ -> ([], [], [])

isBoundedBy :: (Int, Int) -> Int -> Bool
isBoundedBy (lower, upper) target =
  lower <= target && target <= upper

isValidByRule :: Int -> Rule -> Bool
isValidByRule value Rule {range1 = range1, range2 = range2} =
  isBoundedBy range1 value || isBoundedBy range2 value

anyValid :: [Rule] -> Int -> Bool
anyValid rules value = any (isValidByRule value) rules

findErrorRate :: ([Rule], [Int], [[Int]]) -> Int
findErrorRate (rules, _, nearby) =
  sum $ filter (not . anyValid rules) $ concat nearby

partOne :: String -> IO ()
partOne filePath = do
  contents <- readFile filePath
  putStrLn $ ("Part One: " ++) $ show $ findErrorRate $ parse contents

converge :: Eq a => (a -> a) -> a -> a
converge = until =<< ((==) =<<)

convergeRules :: [[Rule]] -> [[Rule]]
convergeRules candidates =
  let singletons :: [Rule]
      singletons = concat $ filter ((1 ==) . length) candidates

      removeRule :: [Rule] -> [Rule]
      removeRule candidate =
        if length candidate == 1
          then candidate
          else filter (`notElem` singletons) candidate
   in map removeRule candidates

determineColumns :: [Rule] -> [[Int]] -> [String]
determineColumns rules validTickets =
  let columns = transpose validTickets

      ruleIsValidForRow :: [Int] -> Rule -> Bool
      ruleIsValidForRow row rule = all (`isValidByRule` rule) row

      columnCandidates :: [[Rule]]
      columnCandidates = (\values -> filter (ruleIsValidForRow values) rules) <$> columns
   in map field $ concat $ converge convergeRules columnCandidates

allValid :: [Rule] -> [Int] -> Bool
allValid rules = all (anyValid rules)

findMyTicketColumns :: ([Rule], [Int], [[Int]]) -> [(String, Int)]
findMyTicketColumns (rules, myTicket, nearby) =
  zip (determineColumns rules $ filter (allValid rules) nearby) myTicket

productOfDeparture :: [(String, Int)] -> Int
productOfDeparture =
  product . map snd . filter (isPrefixOf "departure" . fst)

partTwo :: String -> IO ()
partTwo filePath = do
  contents <- readFile filePath
  putStrLn $ ("Part Two: " ++) $ show $ productOfDeparture $ findMyTicketColumns $ parse contents
