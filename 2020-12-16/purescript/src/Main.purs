module Main where

import Prelude
import Data.Maybe (Maybe(..))
import Data.Array (mapMaybe, any, all, concat, head, tail, filter, foldl, uncons, zip, length, cons, notElem)
import Data.String (split, Pattern(..), contains)
import Data.Int (fromString, toNumber)
import Data.Map as Map
import Effect (Effect)
import Effect.Console (log)
import Node.FS.Sync (readTextFile)
import Node.Encoding (Encoding(..))

type Ticket
  = Array Int

data Range
  = Range Int Int

instance eqRange :: Eq Range where
  eq (Range a b) (Range c d) = a == c && b == d

boundedBy :: Range -> Int -> Boolean
boundedBy (Range l u) n = l <= n && n <= u

infixr 5 boundedBy as <->

type Rule
  = { field :: String
    , range1 :: Range
    , range2 :: Range
    }

isValidByRule :: Rule -> Int -> Boolean
isValidByRule { range1, range2 } n = range1 <-> n || range2 <-> n

infixr 5 isValidByRule as ~>

anyValid :: Array Rule -> Int -> Boolean
anyValid rules value = any (_ ~> value) rules

allValid :: Array Rule -> Ticket -> Boolean
allValid rules = all (anyValid rules)

until :: forall a. Eq a => (a -> Boolean) -> (a -> a) -> a -> a
until p f x = if p x then x else until p f (f x)

converge :: forall a. Eq a => (a -> a) -> a -> a
converge f x = until (\n -> n == f n) f x

transpose :: forall a. Array (Array a) -> Array (Array a)
transpose l = case uncons l of
  Nothing -> []
  Just { head: l', tail: xss } -> case uncons l' of
    Nothing -> transpose xss
    Just { head: x, tail: xs } ->
      (x `cons` mapMaybe head xss)
        `cons`
          transpose (xs `cons` mapMaybe tail xss)

type ProblemInputs
  = { rules :: Array Rule
    , ticket :: Ticket
    , nearby :: Array Ticket
    }

parseRange :: String -> Maybe Range
parseRange raw = case split (Pattern "-") raw of
  [ lower, upper ] -> Range <$> fromString lower <*> fromString upper
  _ -> Nothing

parseRule :: String -> Maybe Rule
parseRule raw = case split (Pattern ": ") raw of
  [ field, rest ] -> case split (Pattern " or ") rest of
    [ range1, range2 ] -> case parseRange <$> [ range1, range2 ] of
      [ Just r1, Just r2 ] -> Just $ { field: field, range1: r1, range2: r2 }
      _ -> Nothing
    _ -> Nothing
  _ -> Nothing

parseRules :: String -> Array Rule
parseRules = mapMaybe parseRule <<< split (Pattern "\n")

parseTicket :: String -> Ticket
parseTicket ticket = mapMaybe fromString $ split (Pattern ",") ticket

parseMyTicket :: String -> Ticket
parseMyTicket raw = case split (Pattern "\n") raw of
  [ _, ticket ] -> parseTicket ticket
  _ -> []

parseNearby :: String -> Array Ticket
parseNearby raw = case tail $ split (Pattern "\n") raw of
  Just rest -> parseTicket <$> rest
  Nothing -> []

parse :: String -> ProblemInputs
parse raw = case split (Pattern "\n\n") raw of
  [ rules, ticket, nearby ] -> { rules: parseRules rules, ticket: parseMyTicket ticket, nearby: parseNearby nearby }
  _ -> { rules: [], ticket: [], nearby: [] }

findErrorRate :: ProblemInputs -> Int
findErrorRate { rules, nearby } =
  nearby
    # concat
    # filter (not <<< anyValid rules)
    # foldl (+) 0

partOne :: String -> Int
partOne = parse >>> findErrorRate

convergeRules :: Array (Array Rule) -> Array (Array Rule)
convergeRules candidates =
  let
    singletons = concat $ filter ((_ == 1) <<< length) candidates

    removeRule :: Array Rule -> Array Rule
    removeRule candidate =
      if length candidate == 1 then
        candidate
      else
        filter (_ `notElem` singletons) candidate
  in
    removeRule <$> candidates

determineColumns :: Array Rule -> Array Ticket -> Array String
determineColumns rules validTickets =
  let
    validateRowRule :: Array Int -> Rule -> Boolean
    validateRowRule ticket rule = all (rule ~> _) ticket

    columnCandidates :: Array (Array Rule)
    columnCandidates = (\values -> filter (validateRowRule values) rules) <$> transpose validTickets
  in
    map _.field $ concat $ converge convergeRules columnCandidates

findMyTicket :: ProblemInputs -> Map.Map String Int
findMyTicket { rules, ticket, nearby } = Map.fromFoldable $ zip (determineColumns rules $ filter (allValid rules) nearby) ticket

productOfDeparture :: Map.Map String Int -> Number
productOfDeparture =
  Map.filterKeys (contains (Pattern "departure"))
    >>> Map.values
    >>> map toNumber
    >>> foldl (*) (toNumber 1)

partTwo :: String -> Number
partTwo = parse >>> findMyTicket >>> productOfDeparture

main :: Effect Unit
main = do
  contents <- readTextFile UTF8 "../2020-12-16.txt"
  log $ "Part One: " <> (show $ partOne contents)
  log $ "Part Two: " <> (show $ partTwo contents)
