module Lib
  ( partOne,
    partTwo,
    evaluate,
  )
where

import Data.List

slice :: Int -> Int -> [a] -> [a]
slice from to xs = take (to - from + 1) (drop from xs)

evaluate :: String -> Int
evaluate expr =
  if '(' `elem` expr
    then
      let rightMostOpen = maximum $ elemIndices '(' expr
          leftMostClose = head $ filter (> rightMostOpen) $ elemIndices ')' expr
          result = evaluate $ slice (rightMostOpen + 1) leftMostClose expr
       in evaluate $ slice 0 rightMostOpen expr ++ show result ++ slice (leftMostClose + 1) (length expr) expr
    else case words expr of
      (first : op : second : rest) ->
        if op == "+"
          then evaluate $ unwords $ show (read first + read second) : rest
          else evaluate $ unwords $ show (read first * read second) : rest
      [a] -> read a

partOne :: String -> IO ()
partOne filePath = do
  contents <- readFile filePath
  putStrLn $ "Part One: " ++ show (sum $ map evaluate $ lines contents)

partTwo :: String -> IO ()
partTwo filePath = do
  contents <- readFile filePath
  putStrLn $ "Part Two: " ++ show (-1)
