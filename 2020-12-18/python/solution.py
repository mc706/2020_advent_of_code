from typing import List

def get_data() -> List[str]:
    with open('../2020-12-18.txt', 'r') as input_file:
        return input_file.read().split('\n')


def evaluate_expression(expr: str) -> int:
    """
    >>> evaluate_expression("2 * 3 + (4 * 5)")
    26
    >>> evaluate_expression("5 + (8 * 3 + 9 + 3 * 4 * 3)")
    437
    >>> evaluate_expression("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")
    12240
    >>> evaluate_expression("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")
    13632
    """
    while '(' in expr:
        start = 0
        for i, c in enumerate(expr):
            if c == '(':
                start = i
            if c == ')':
                expr = expr[0:start] + str(evaluate_expression(expr[start+1:i])) + expr[i+1:]
                break
    
    terms = expr.split(' ')
    while len(terms) >= 3:
        first, op, second, *terms = terms
        result = int(first) + int(second) if op == '+' else int(first) * int(second)
        terms.insert(0, str(result))
    return int(terms[0])


def evaluate_expression_pres(expr: str) -> int:
    """
    >>> evaluate_expression_pres("2 * 3 + (4 * 5)")
    46
    >>> evaluate_expression_pres("5 + (8 * 3 + 9 + 3 * 4 * 3)")
    1445
    >>> evaluate_expression_pres("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")
    669060
    >>> evaluate_expression_pres("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")
    23340
    """   
    while '(' in expr or ')' in expr:
        start = 0
        for i, c in enumerate(expr):
            if c == '(':
                start = i
            if c == ')':
                expr = expr[0:start] + str(evaluate_expression_pres(expr[start+1:i])) + expr[i+1:]
                break
    
    terms = expr.split(' ')
    while len(terms) >= 5:
        first, op1, second, op2, third, *terms = terms
        if op1 == '+':
            result = int(first) + int(second)
            terms = [f"{result}", op2, third] + terms
        elif op2 == '+':
            result = int(second) + int(third)
            terms = [first, op1, f"{result}"] + terms
        else:
            result = int(first) * int(second)
            terms = [f"{result}", op2, third] + terms
    
    first, op, second = terms
    result = int(first) + int(second) if op == '+' else int(first) * int(second)
    return result


if __name__ == "__main__":
    import doctest
    doctest.testmod()
    print("Part One: ", sum(map(evaluate_expression, get_data())))
    print("Part Two: ", sum(map(evaluate_expression_pres, get_data())))
