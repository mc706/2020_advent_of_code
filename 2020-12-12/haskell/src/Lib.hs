module Lib
  ( partOne,
    partTwo,
  )
where

import Data.Maybe

data Instruction
  = N Int
  | S Int
  | E Int
  | W Int
  | L Int
  | R Int
  | F Int

parse :: String -> Maybe Instruction
parse (h : num) =
  case h of
    'N' -> Just $ N (read num)
    'S' -> Just $ S (read num)
    'E' -> Just $ E (read num)
    'W' -> Just $ W (read num)
    'L' -> Just $ L (read num)
    'R' -> Just $ R (read num)
    'F' -> Just $ F (read num)
    _ -> Nothing

data Position = Position {x :: Int, y :: Int, heading :: Int}

apply :: Position -> Instruction -> Position
apply pos inst =
  case inst of
    N v -> pos {y = y pos + v}
    S v -> pos {y = y pos - v}
    W v -> pos {x = x pos - v}
    E v -> pos {x = x pos + v}
    L v -> pos {heading = heading pos - v}
    R v -> pos {heading = heading pos + v}
    F v ->
      case ((heading pos `mod` 360) + 360) `mod` 360 of
        0 -> apply pos (E v)
        90 -> apply pos (S v)
        180 -> apply pos (W v)
        270 -> apply pos (N v)
        _ -> pos

findDestination :: [Instruction] -> Position
findDestination =
  foldl apply (Position 0 0 0)

calculateTaxiCab :: Position -> Int
calculateTaxiCab Position {x = x, y = y} = abs x + abs y

partOne :: String -> IO ()
partOne filePath = do
  contents <- readFile filePath
  putStrLn $ ("Part One: " ++) $ show $ calculateTaxiCab . findDestination $ mapMaybe parse $ lines contents

data WayPoint = WayPoint {dx :: Int, dy :: Int}
  deriving (Show)

rotate :: WayPoint -> WayPoint
rotate wyp = WayPoint (dy wyp) (-1 * dx wyp)

move :: (Position, WayPoint) -> (Position, WayPoint)
move (pos@Position {x = x, y = y}, wyp@WayPoint {dx = dx, dy = dy}) =
  (pos {x = x + dx, y = y + dy}, wyp)

applyWayPoint :: (Position, WayPoint) -> Instruction -> (Position, WayPoint)
applyWayPoint (pos, wyp) inst =
  case inst of
    N v -> (pos, wyp {dy = dy wyp + v})
    S v -> (pos, wyp {dy = dy wyp - v})
    W v -> (pos, wyp {dx = dx wyp - v})
    E v -> (pos, wyp {dx = dx wyp + v})
    L v -> (pos, iterate rotate wyp !! ((360 - v) `div` 90))
    R v -> (pos, iterate rotate wyp !! (v `div` 90))
    F v -> iterate move (pos, wyp) !! v

findWaypointDestination :: [Instruction] -> Position
findWaypointDestination =
  fst . foldl applyWayPoint (Position 0 0 0, WayPoint 10 1)

partTwo :: String -> IO ()
partTwo filePath = do
  contents <- readFile filePath
  putStrLn $ ("Part Two: " ++) $ show $ calculateTaxiCab . findWaypointDestination $ mapMaybe parse $ lines contents
