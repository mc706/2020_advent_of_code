module Main where

import Prelude
import Data.Maybe (Maybe(..))
import Data.Function (applyN)
import Data.Int (fromString)
import Data.Array (foldl, mapMaybe)
import Data.String (split, uncons)
import Data.String.Pattern (Pattern(..))
import Data.String.CodePoints (singleton)
import Data.Ord (abs)
import Effect (Effect)
import Effect.Console (log)
import Node.FS.Sync (readTextFile)
import Node.Encoding (Encoding(..))

data Instruction
  = N Int
  | S Int
  | E Int
  | W Int
  | L Int
  | R Int
  | F Int

parse :: String -> Maybe Instruction
parse raw = case uncons raw of
  Nothing -> Nothing
  Just { head: h, tail: num } -> case singleton h of
    "N" -> N <$> (fromString num)
    "S" -> S <$> (fromString num)
    "W" -> W <$> (fromString num)
    "E" -> E <$> (fromString num)
    "L" -> L <$> (fromString num)
    "R" -> R <$> (fromString num)
    "F" -> F <$> (fromString num)
    _ -> Nothing

type Position
  = { x :: Int
    , y :: Int
    , heading :: Int
    }

applyInstruction :: Position -> Instruction -> Position
applyInstruction pos@{ x, y, heading } inst = case inst of
  N v -> pos { y = y + v }
  S v -> pos { y = y - v }
  W v -> pos { x = x - v }
  E v -> pos { x = x + v }
  L v -> pos { heading = heading - v }
  R v -> pos { heading = heading + v }
  F v -> case ((heading `mod` 360) + 360) `mod` 360 of
    0 -> applyInstruction pos (E v)
    90 -> applyInstruction pos (S v)
    180 -> applyInstruction pos (W v)
    270 -> applyInstruction pos (N v)
    _ -> pos

findDestination :: Array Instruction -> Position
findDestination = foldl applyInstruction { x: 0, y: 0, heading: 0 }

calculateTaxiCab :: Position -> Int
calculateTaxiCab { x, y } = abs x + abs y

splitOnNewlines :: String -> Array String
splitOnNewlines = split (Pattern "\n")

partOne :: String -> Int
partOne =
  splitOnNewlines
    >>> mapMaybe parse
    >>> findDestination
    >>> calculateTaxiCab

type WayPoint
  = { dx :: Int
    , dy :: Int
    }

type Bearing
  = { position :: Position
    , waypoint :: WayPoint
    }

rotate :: WayPoint -> WayPoint
rotate { dx, dy } = { dx: dy, dy: -dx }

move :: Bearing -> Bearing
move { position: pos@{ x, y }, waypoint: wyp@{ dx, dy } } =
  { position: pos { x = x + dx, y = y + dy }
  , waypoint: wyp
  }

applyWayPointInstruction :: Bearing -> Instruction -> Bearing
applyWayPointInstruction bearing@{ waypoint } inst = case inst of
  N v -> bearing { waypoint = waypoint { dy = waypoint.dy + v } }
  S v -> bearing { waypoint = waypoint { dy = waypoint.dy - v } }
  W v -> bearing { waypoint = waypoint { dx = waypoint.dx - v } }
  E v -> bearing { waypoint = waypoint { dx = waypoint.dx + v } }
  L v -> bearing { waypoint = applyN rotate ((360 - v) `div` 90) waypoint }
  R v -> bearing { waypoint = applyN rotate (v `div` 90) waypoint }
  F v -> applyN move v bearing

findWayPointDestination :: Array Instruction -> Position
findWayPointDestination =
  foldl applyWayPointInstruction { position: { x: 0, y: 0, heading: 0 }, waypoint: { dx: 10, dy: 1 } }
    >>> _.position

partTwo :: String -> Int
partTwo =
  splitOnNewlines
    >>> mapMaybe parse
    >>> findWayPointDestination
    >>> calculateTaxiCab

main :: Effect Unit
main = do
  contents <- readTextFile UTF8 "../2020-12-12.txt"
  log $ "Part One: " <> (show $ partOne contents)
  log $ "Part Two: " <> (show $ partTwo contents)
