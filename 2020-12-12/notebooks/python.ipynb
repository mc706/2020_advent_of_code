{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your ferry made decent progress toward the island, but the storm came in faster than anyone expected. The ferry needs to take evasive actions!\n",
    "\n",
    "Unfortunately, the ship's navigation computer seems to be malfunctioning; rather than giving a route directly to safety, it produced extremely circuitous instructions. When the captain uses the PA system to ask if anyone can help, you quickly volunteer.\n",
    "\n",
    "The navigation instructions (your puzzle input) consists of a sequence of single-character actions paired with integer input values. After staring at them for a few minutes, you work out what they probably mean:\n",
    "\n",
    "* Action N means to move north by the given value.\n",
    "* Action S means to move south by the given value.\n",
    "* Action E means to move east by the given value.\n",
    "* Action W means to move west by the given value.\n",
    "* Action L means to turn left the given number of degrees.\n",
    "* Action R means to turn right the given number of degrees.\n",
    "* Action F means to move forward by the given value in the direction the ship is currently facing.\n",
    "The ship starts by facing east. Only the L and R actions change the direction the ship is facing. (That is, if the ship is facing east and the next instruction is N10, the ship would move north 10 units, but would still move east if the following action were F.)\n",
    "\n",
    "For example:\n",
    "```\n",
    "F10\n",
    "N3\n",
    "F7\n",
    "R90\n",
    "F11\n",
    "```\n",
    "These instructions would be handled as follows:\n",
    "\n",
    "* F10 would move the ship 10 units east (because the ship starts by facing east) to east 10, north 0.\n",
    "* N3 would move the ship 3 units north to east 10, north 3.\n",
    "* F7 would move the ship another 7 units east (because the ship is still facing east) to east 17, north 3.\n",
    "* R90 would cause the ship to turn right by 90 degrees and face south; it remains at east 17, north 3.\n",
    "* F11 would move the ship 11 units south to east 17, south 8.\n",
    "At the end of these instructions, the ship's Manhattan distance (sum of the absolute values of its east/west position and its north/south position) from its starting position is 17 + 8 = 25.\n",
    "\n",
    "Figure out where the navigation instructions lead. What is the Manhattan distance between that location and the ship's starting position?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Iterable\n",
    "\n",
    "def get_data() -> Iterable[str]:\n",
    "    with open('2020-12-12.txt', 'r') as input_file:\n",
    "        return input_file.readlines()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from enum import Enum\n",
    "\n",
    "class Action(Enum):\n",
    "    N = 'N'\n",
    "    S = 'S'\n",
    "    E = 'E'\n",
    "    W = 'W'\n",
    "    L = 'L'\n",
    "    R = 'R'\n",
    "    F = 'F'\n",
    "    \n",
    "from dataclasses import dataclass\n",
    "\n",
    "@dataclass(frozen=True)\n",
    "class Instruction:\n",
    "    action: Action\n",
    "    value: int\n",
    "        \n",
    "def parse(raw: str) -> Instruction:\n",
    "    return Instruction(Action(raw[0]), int(raw[1:]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "@dataclass(frozen=True)\n",
    "class Position:\n",
    "    x: int = 0\n",
    "    y: int = 0\n",
    "    heading: int = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Callable\n",
    "from functools import reduce\n",
    "from dataclasses import replace\n",
    "from operator import add, sub\n",
    "\n",
    "def apply(prop: str, operation: Callable[[int, int], int]) -> Callable[[Position, Instruction], Position]:\n",
    "    def _inner(position: Position, instruction: Instruction) -> Position:\n",
    "        return replace(position, **{prop: operation(getattr(position, prop), instruction.value)})\n",
    "    return _inner\n",
    "\n",
    "bearing_map =  {\n",
    "    0: Action.E,\n",
    "    90: Action.S,\n",
    "    180: Action.W,\n",
    "    270: Action.N,\n",
    "}\n",
    "\n",
    "def move_forward(position: Position, instruction: Instruction) -> Position:\n",
    "    normalized = ((position.heading % 360) + 360 ) % 360\n",
    "    direction = bearing_map[normalized]\n",
    "    return action_map[direction](position, instruction)\n",
    "    \n",
    "action_map = {\n",
    "    Action.N: apply('y', add),\n",
    "    Action.S: apply('y', sub),\n",
    "    Action.W: apply('x', sub),\n",
    "    Action.E: apply('x', add),\n",
    "    Action.R: apply('heading', add),\n",
    "    Action.L: apply('heading', sub),\n",
    "    Action.F: move_forward,\n",
    "}\n",
    "\n",
    "def apply_instruction(position: Position, instruction: Instruction) -> Position:\n",
    "    return action_map[instruction.action](position, instruction)\n",
    "        \n",
    "\n",
    "def find_taxi_cab_destination(data: Iterable[str]) -> int:\n",
    "    final_position = reduce(apply_instruction, map(parse, data), Position())\n",
    "    return abs(final_position.x) + abs(final_position.y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "sample_data = ['F10','N3','F7','R90','F11']\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "25"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "find_taxi_cab_destination(iter(sample_data))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1956"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "find_taxi_cab_destination(get_data())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before you can give the destination to the captain, you realize that the actual action meanings were printed on the back of the instructions the whole time.\n",
    "\n",
    "Almost all of the actions indicate how to move a waypoint which is relative to the ship's position:\n",
    "\n",
    "* Action N means to move the waypoint north by the given value.\n",
    "* Action S means to move the waypoint south by the given value.\n",
    "* Action E means to move the waypoint east by the given value.\n",
    "* Action W means to move the waypoint west by the given value.\n",
    "* Action L means to rotate the waypoint around the ship left (counter-clockwise) the given number of degrees.\n",
    "* Action R means to rotate the waypoint around the ship right (clockwise) the given number of degrees.\n",
    "* Action F means to move forward to the waypoint a number of times equal to the given value.\n",
    "The waypoint starts 10 units east and 1 unit north relative to the ship. The waypoint is relative to the ship; that is, if the ship moves, the waypoint moves with it.\n",
    "\n",
    "For example, using the same instructions as above:\n",
    "\n",
    "* F10 moves the ship to the waypoint 10 times (a total of 100 units east and 10 units north), leaving the ship at east 100, north 10. The waypoint stays 10 units east and 1 unit north of the ship.\n",
    "* N3 moves the waypoint 3 units north to 10 units east and 4 units north of the ship. The ship remains at east 100, north 10.\n",
    "* F7 moves the ship to the waypoint 7 times (a total of 70 units east and 28 units north), leaving the ship at east 170, north 38. The waypoint stays 10 units east and 4 units north of the ship.\n",
    "* R90 rotates the waypoint around the ship clockwise 90 degrees, moving it to 4 units east and 10 units south of the ship. The ship remains at east 170, north 38.\n",
    "* F11 moves the ship to the waypoint 11 times (a total of 44 units east and 110 units south), leaving the ship at east 214, south 72. The waypoint stays 4 units east and 10 units south of the ship.\n",
    "After these operations, the ship's Manhattan distance from its starting position is 214 + 72 = 286.\n",
    "\n",
    "\n",
    "Figure out where the navigation instructions actually lead. What is the Manhattan distance between that location and the ship's starting position?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Tuple \n",
    "\n",
    "@dataclass(frozen=True)\n",
    "class WayPoint:\n",
    "    x: int = 0\n",
    "    y: int = 0\n",
    "\n",
    "def apply_waypoint(prop: str, operation: Callable[[int, int], int]) -> Callable[[Tuple[Position, WayPoint], Instruction], Position]:\n",
    "    def _inner(state: Tuple[Position, WayPoint], instruction: Instruction) -> Tuple[Position, WayPoint]:\n",
    "        position, waypoint = state\n",
    "        return position, replace(waypoint, **{prop: operation(getattr(waypoint, prop), instruction.value)})\n",
    "    return _inner\n",
    "        \n",
    "def rotate_waypoint(state: Tuple[Position, WayPoint], instruction: Instruction) -> Tuple[Position, WayPoint]:\n",
    "    position, waypoint = state\n",
    "    rotation = (instruction.value if instruction.action == Action.R else 360 - instruction.value) // 90\n",
    "    for _ in range(rotation):\n",
    "        waypoint = replace(waypoint, x=waypoint.y, y = waypoint.x * -1)\n",
    "    return position, waypoint\n",
    "\n",
    "def move_to_waypoint(state: Tuple[Position, WayPoint], instruction: Instruction) -> Tuple[Position, WayPoint]:\n",
    "    position, waypoint = state\n",
    "    return replace(position, x=position.x + (waypoint.x * instruction.value), y=position.y + (waypoint.y * instruction.value)), waypoint\n",
    "        \n",
    "waypoint_action_map = {\n",
    "    Action.N: apply_waypoint('y', add),\n",
    "    Action.S: apply_waypoint('y', sub),\n",
    "    Action.W: apply_waypoint('x', sub),\n",
    "    Action.E: apply_waypoint('x', add),\n",
    "    Action.R: rotate_waypoint,\n",
    "    Action.L: rotate_waypoint,\n",
    "    Action.F: move_to_waypoint,\n",
    "}  \n",
    "\n",
    "def apply_waypoint_instruction(state: Tuple[Position, WayPoint], instruction: Instruction) -> Tuple[Position, WayPoint]:\n",
    "    return waypoint_action_map[instruction.action](state, instruction)\n",
    "        \n",
    "def find_taxi_cab_destination_wapypoint(data: Iterable[str]) -> int:\n",
    "    final_position, _ = reduce(apply_waypoint_instruction, map(parse, data), (Position(), WayPoint(10, 1)))\n",
    "    return abs(final_position.x) + abs(final_position.y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "286"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "find_taxi_cab_destination_wapypoint(sample_data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "126797"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "find_taxi_cab_destination_wapypoint(get_data())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
