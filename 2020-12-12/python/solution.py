from enum import Enum
from typing import Iterable, Callable, Tuple
from dataclasses import dataclass, replace
from functools import reduce
from operator import add, sub

def get_data() -> Iterable[str]:
    with open('../2020-12-12.txt', 'r') as input_file:
        return input_file.readlines()

class Action(Enum):
    N = 'N'
    S = 'S'
    E = 'E'
    W = 'W'
    L = 'L'
    R = 'R'
    F = 'F'

@dataclass(frozen=True)
class Instruction:
    action: Action
    value: int
        
def parse(raw: str) -> Instruction:
    return Instruction(Action(raw[0]), int(raw[1:]))

@dataclass(frozen=True)
class Position:
    x: int = 0
    y: int = 0
    heading: int = 0

def apply(prop: str, operation: Callable[[int, int], int]) -> Callable[[Position, Instruction], Position]:
    def _inner(position: Position, instruction: Instruction) -> Position:
        return replace(position, **{prop: operation(getattr(position, prop), instruction.value)})
    return _inner

bearing_map =  {
    0: Action.E,
    90: Action.S,
    180: Action.W,
    270: Action.N,
}

def move_forward(position: Position, instruction: Instruction) -> Position:
    normalized = ((position.heading % 360) + 360 ) % 360
    direction = bearing_map[normalized]
    return action_map[direction](position, instruction)
    
action_map = {
    Action.N: apply('y', add),
    Action.S: apply('y', sub),
    Action.W: apply('x', sub),
    Action.E: apply('x', add),
    Action.R: apply('heading', add),
    Action.L: apply('heading', sub),
    Action.F: move_forward,
}

def apply_instruction(position: Position, instruction: Instruction) -> Position:
    return action_map[instruction.action](position, instruction)
        
def find_taxi_cab_destination(data: Iterable[str]) -> int:
    final_position = reduce(apply_instruction, map(parse, data), Position())
    return abs(final_position.x) + abs(final_position.y)

@dataclass(frozen=True)
class WayPoint:
    x: int = 0
    y: int = 0

def apply_waypoint(prop: str, operation: Callable[[int, int], int]) -> Callable[[Tuple[Position, WayPoint], Instruction], Position]:
    def _inner(state: Tuple[Position, WayPoint], instruction: Instruction) -> Tuple[Position, WayPoint]:
        position, waypoint = state
        return position, replace(waypoint, **{prop: operation(getattr(waypoint, prop), instruction.value)})
    return _inner
        
def rotate_waypoint(state: Tuple[Position, WayPoint], instruction: Instruction) -> Tuple[Position, WayPoint]:
    position, waypoint = state
    rotation = (instruction.value if instruction.action == Action.R else 360 - instruction.value) // 90
    for _ in range(rotation):
        waypoint = replace(waypoint, x=waypoint.y, y = waypoint.x * -1)
    return position, waypoint

def move_to_waypoint(state: Tuple[Position, WayPoint], instruction: Instruction) -> Tuple[Position, WayPoint]:
    position, waypoint = state
    return replace(position, x=position.x + (waypoint.x * instruction.value), y=position.y + (waypoint.y * instruction.value)), waypoint
        
waypoint_action_map = {
    Action.N: apply_waypoint('y', add),
    Action.S: apply_waypoint('y', sub),
    Action.W: apply_waypoint('x', sub),
    Action.E: apply_waypoint('x', add),
    Action.R: rotate_waypoint,
    Action.L: rotate_waypoint,
    Action.F: move_to_waypoint,
}  

def apply_waypoint_instruction(state: Tuple[Position, WayPoint], instruction: Instruction) -> Tuple[Position, WayPoint]:
    return waypoint_action_map[instruction.action](state, instruction)
        
def find_taxi_cab_destination_wapypoint(data: Iterable[str]) -> int:
    final_position, _ = reduce(apply_waypoint_instruction, map(parse, data), (Position(), WayPoint(10, 1)))
    return abs(final_position.x) + abs(final_position.y)

if __name__ == "__main__":
    find_taxi_cab_destination(get_data())
    find_taxi_cab_destination_wapypoint(get_data())