{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You land at the regional airport in time for your next flight. In fact, it looks like you'll even have time to grab some food: all flights are currently delayed due to issues in luggage processing.\n",
    "\n",
    "Due to recent aviation regulations, many rules (your puzzle input) are being enforced about bags and their contents; bags must be color-coded and must contain specific quantities of other color-coded bags. Apparently, nobody responsible for these regulations considered how long they would take to enforce!\n",
    "\n",
    "For example, consider the following rules:\n",
    "```\n",
    "light red bags contain 1 bright white bag, 2 muted yellow bags.\n",
    "dark orange bags contain 3 bright white bags, 4 muted yellow bags.\n",
    "bright white bags contain 1 shiny gold bag.\n",
    "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.\n",
    "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.\n",
    "dark olive bags contain 3 faded blue bags, 4 dotted black bags.\n",
    "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.\n",
    "faded blue bags contain no other bags.\n",
    "dotted black bags contain no other bags.\n",
    "```\n",
    "These rules specify the required contents for 9 bag types. In this example, every faded blue bag is empty, every vibrant plum bag contains 11 bags (5 faded blue and 6 dotted black), and so on.\n",
    "\n",
    "You have a shiny gold bag. If you wanted to carry it in at least one other bag, how many different bag colors would be valid for the outermost bag? (In other words: how many colors can, eventually, contain at least one shiny gold bag?)\n",
    "\n",
    "In the above rules, the following options would be available to you:\n",
    "\n",
    "* A bright white bag, which can hold your shiny gold bag directly.\n",
    "* A muted yellow bag, which can hold your shiny gold bag directly, plus some other bags.\n",
    "* A dark orange bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.\n",
    "* A light red bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.\n",
    "So, in this example, the number of bag colors that can eventually contain at least one shiny gold bag is 4.\n",
    "\n",
    "How many bag colors can eventually contain at least one shiny gold bag? (The list of rules is quite long; make sure you get all of it.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Tuple, Dict, Set"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_rules() -> Tuple[Dict[str, Dict[str, int]], Dict[str, Set[str]]]:\n",
    "    mapping = {}\n",
    "    reverse_mapping = {}\n",
    "    with open('2020-12-07.txt', 'r') as input_files:\n",
    "        for line in input_files.readlines():\n",
    "            subject, rules = line.split(' bags contain ')\n",
    "            if 'no other bags' not in rules:\n",
    "                cleaned = rules.replace('.', '')\n",
    "                mapping[subject] = {}\n",
    "                for phrase in cleaned.split(', '):\n",
    "                    count, *target, _ = phrase.split(' ')\n",
    "                    target_name = \" \".join(target)\n",
    "                    mapping[subject][target_name] = int(count)\n",
    "                    reverse_mapping.setdefault(target_name, set()).add(subject)\n",
    "            else:\n",
    "                mapping[subject] = {}\n",
    "    return mapping, reverse_mapping"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def count_potential_parents(reverse_mapping: Dict[str, Set[str]], target: str) -> int:\n",
    "    parents = set()\n",
    "    checked = list(reverse_mapping.get(target, set()))\n",
    "    while checked:\n",
    "        parent = checked.pop()\n",
    "        parents.add(parent)\n",
    "        checked += [p for p in reverse_mapping.get(parent, set()) if p not in parents]\n",
    "    return len(parents)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "mapping, reverse_mapping = get_rules()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "287"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "count_potential_parents(reverse_mapping, 'shiny gold')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's getting pretty expensive to fly these days - not because of ticket prices, but because of the ridiculous number of bags you need to buy!\n",
    "\n",
    "Consider again your shiny gold bag and the rules from the above example:\n",
    "\n",
    "* faded blue bags contain 0 other bags.\n",
    "* dotted black bags contain 0 other bags.\n",
    "* vibrant plum bags contain 11 other bags: 5 faded blue bags and 6 dotted black bags.\n",
    "* dark olive bags contain 7 other bags: 3 faded blue bags and 4 dotted black bags.\n",
    "So, a single shiny gold bag must contain 1 dark olive bag (and the 7 bags within it) plus 2 vibrant plum bags (and the 11 bags within each of those): 1 + 1*7 + 2 + 2*11 = 32 bags!\n",
    "\n",
    "Of course, the actual rules have a small chance of going several levels deeper than this example; be sure to count all of the bags, even if the nesting becomes topologically impractical!\n",
    "\n",
    "Here's another example:\n",
    "```\n",
    "shiny gold bags contain 2 dark red bags.\n",
    "dark red bags contain 2 dark orange bags.\n",
    "dark orange bags contain 2 dark yellow bags.\n",
    "dark yellow bags contain 2 dark green bags.\n",
    "dark green bags contain 2 dark blue bags.\n",
    "dark blue bags contain 2 dark violet bags.\n",
    "dark violet bags contain no other bags.\n",
    "```\n",
    "In this example, a single shiny gold bag must contain 126 other bags.\n",
    "\n",
    "How many individual bags are required inside your single shiny gold bag?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "def count_children(mapping: Dict[str, Dict[str, int]], target: str) -> int:\n",
    "    return sum(\n",
    "        count + count * count_children(mapping, child) \n",
    "        for child, count in mapping.get(target, {}).items()\n",
    "    )\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "48160"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "count_children(mapping, 'shiny gold')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
