module Main where

import Prelude
import Data.String (split, Pattern(..))
import Data.Int (fromString)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Array (catMaybes, sortWith, head, mapWithIndex, uncons, foldl)
import Effect (Effect)
import Effect.Console (log)
import Node.FS.Sync (readTextFile)
import Node.Encoding (Encoding(..))

type PuzzleInput
  = { target :: Int
    , schedule :: Array (Maybe Int)
    }

parseInput :: String -> PuzzleInput
parseInput text = case split (Pattern "\n") text of
  [ target, schedule ] -> case fromString target of
    Just target' -> { target: target', schedule: map fromString $ split (Pattern ",") schedule }
    Nothing -> { target: 0, schedule: [] }
  _ -> { target: 0, schedule: [] }

type BusTime
  = { bus :: Int, time :: Int }

findSoonestBus :: PuzzleInput -> BusTime
findSoonestBus { target, schedule } =
  schedule
    # catMaybes
    # map (\bus -> { bus: bus, time: bus - (target `mod` bus) })
    # sortWith _.time
    # head
    # fromMaybe { bus: 0, time: 0 }

busTimeToAnswer :: BusTime -> Int
busTimeToAnswer { bus, time } = bus * time

busTimeFromBus :: Int -> Int -> BusTime
busTimeFromBus offset busId = { bus: busId, time: offset }

partOne :: String -> Int
partOne =
  parseInput
    >>> findSoonestBus
    >>> busTimeToAnswer

extractOffsetSchedule :: PuzzleInput -> Array BusTime
extractOffsetSchedule { schedule } =
  schedule
    # mapWithIndex (\i bus -> map (busTimeFromBus i) bus)
    # catMaybes

until :: forall a. (a -> Boolean) -> (a -> a) -> a -> a
until p f x = if p x then x else until p f (f x)

busTimeLCM :: BusTime -> BusTime -> BusTime
busTimeLCM { bus: base, time: increment } { bus: busId, time: offset } =
  { bus: until (\b -> (b + offset) `mod` busId == 0) ((+) increment) base
  , time: increment * busId
  }

findScheduleAlignment :: Array BusTime -> Int
findScheduleAlignment schedule = case uncons schedule of
  Just { head, tail } -> _.bus $ foldl busTimeLCM { bus: head.bus, time: head.bus } tail
  Nothing -> -1

partTwo :: String -> Int
partTwo =
  parseInput
    >>> extractOffsetSchedule
    >>> findScheduleAlignment

main :: Effect Unit
main = do
  contents <- readTextFile UTF8 "../2020-12-13.txt"
  log $ "Part One: " <> (show $ partOne contents)
  log $ "Part Two: " <> (show $ partTwo contents)
