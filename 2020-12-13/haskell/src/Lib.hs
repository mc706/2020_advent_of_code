module Lib
  ( partOne,
    partTwo,
  )
where

import Data.Foldable (minimumBy)
import Data.Function (on, (&))
import Data.Maybe (catMaybes)

split :: Eq a => a -> [a] -> [[a]]
split _ [] = []
split d s = x : split d (drop 1 y) where (x, y) = span (/= d) s

parseInput :: String -> (Int, [Maybe Int])
parseInput text =
  let parseSchedule :: String -> [Maybe Int]
      parseSchedule schTxt =
        split ',' schTxt
          & map (\x -> if x == "x" then Nothing else Just $ read x)
   in case lines text of
        [target, schedule] -> (read target, parseSchedule schedule)
        _ -> (0, [])

findSoonestBus :: Int -> [Maybe Int] -> (Int, Int)
findSoonestBus target schedule =
  minimumBy (compare `on` snd) $ map (\x -> (x, x - (target `mod` x))) $ catMaybes schedule

partOne :: String -> IO ()
partOne filePath = do
  contents <- readFile filePath
  putStrLn $ "Part One: " <> show (uncurry (*) . uncurry findSoonestBus $ parseInput contents)

lcmReducer :: (Int, Int) -> (Int, Int) -> (Int, Int)
lcmReducer (base, increment) (offset, busId) =
  (until (\b -> (b + offset) `mod` busId == 0) (+ increment) base, increment * busId)

liftTupleSnd :: (a, Maybe b) -> Maybe (a, b)
liftTupleSnd (a, b) =
  case b of
    Nothing -> Nothing
    Just b' -> Just (a, b')

maybeIndex :: [Maybe a] -> [Maybe (Int, a)]
maybeIndex =
  zipWith (curry liftTupleSnd) [0 ..]

findLCMSchedule :: [Maybe Int] -> Int
findLCMSchedule schedule =
  fst $ foldl1 lcmReducer $ catMaybes $ maybeIndex schedule

partTwo :: String -> IO ()
partTwo filePath = do
  contents <- readFile filePath
  putStrLn $ "Part Two: " <> show (findLCMSchedule $ snd $ parseInput contents)