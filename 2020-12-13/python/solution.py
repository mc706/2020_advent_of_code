from typing import Tuple, List, Optional
from functools import reduce
from itertools import count, dropwhile


def get_data() -> Tuple[int, List[Optional[int]]]:
    with open('../2020-12-13.txt', 'r') as input_file:
        first, second = list(input_file.readlines())
    return int(first), list(map(lambda x: int(x) if x != 'x' else None, second.split(',')))

def find_soonest(target: int, buses: List[int]) -> int:
    bus, wait = min(map(lambda x: (x, x - (target % x)), filter(None, buses)), key=lambda x: x[1])
    return bus * wait


def make_counter(f, o):
    return map(lambda x: (x * f) + o, count(1)) 

def find_first_brute_force(buses: List[Optional[int]]) -> int:
    """
    this function wont complete for larger inputs
    """
    bus_schedules = [(i, bus) for i, bus in enumerate(buses) if bus is not None]
    _, first_bus = bus_schedules.pop(0)
    return next(filter(lambda n: all(i == bus - (n % bus) for i, bus in bus_schedules), map(lambda x: x * first_bus, count(1))))

def lcm_reducer(acc: Tuple[int, int], curr: Tuple[int, int]) -> Tuple[int, int]:
    base, increment = acc
    offset, busId = curr
    if base == busId: return acc 
    while (base + offset) % busId:
        base += increment 
    increment *= busId 
    return base, increment

def find_first(buses: List[Optional[int]]) -> int:
    bus_schedules = [(i, bus) for i, bus in enumerate(buses) if bus is not None]
    return reduce(lcm_reducer, bus_schedules)[0]

if __name__ == "__main__":
    print("Part One: ", find_soonest(*get_data()))
    _, buses = get_data()
    print('Part Two: ', find_first(buses))
