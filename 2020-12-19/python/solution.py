from typing import List, Tuple, Dict, Optional, Union
from enum import Enum, auto
from dataclasses import dataclass
from itertools import product, chain
from functools import lru_cache, cached_property

def get_data() -> Tuple[List[str], List[str]]:
    with open("../2020-12-19.txt","r") as input_file:
        rules, message = input_file.read().split('\n\n')
        return rules.split('\n'), message.split('\n')

def cross_product_sum(a, b):
    return tuple(x + y for x, y in product(a, b))

class hashabledict(dict):
    def __hash__(self):
        return hash(tuple(sorted(self.items())))

@dataclass(frozen=True)
class Rule:
    i: Optional[int] = None
    literal: Optional[str] = None
    ref: Optional[int] = None
    union: Optional[Tuple['Rule']] = None 
    combo: Optional[Tuple['Rule']] = None 

    @cached_property
    def kind(self) -> str:
        if self.literal is not None: return "Literal"
        if self.ref is not None: return "Ref"
        if self.combo is not None: return "Combo"
        if self.union is not None: return "Union"
        return 'Nil'

    @cached_property
    def unpack(self) -> Tuple[str, Union[str, int, Tuple['Rule']]]:
        return (self.kind, abs(self))

    def __repr__(self) -> str:
        return f'<{self.kind}: {abs(self)}>'

    def __abs__(self) -> Union[str, int, Tuple['Rule']]:
        return next(filter(None, [self.literal, self.ref, self.combo, self.union]), None)

    @classmethod
    def parse(cls, raw: str, index: Optional[int] = None) -> 'Rule':
        if '|' in raw:
            left, right = raw.split(' | ')
            return cls(i = index, union=(cls.parse(left), cls.parse(right)))
        if ' ' in raw:
            refs = raw.split(' ')
            return cls(i=index, combo=tuple(map(cls.parse, refs)))
        if raw.isnumeric():
            return cls(i=index, ref=int(raw))
        return cls(i=index, literal=raw.replace('"', ''))

    @classmethod
    def parse_raw(cls, raw: str) -> Tuple[int, "Rule"]:
        i, rest = raw.split(': ')
        return int(i), cls.parse(rest, index=i)

    @classmethod
    def valid_set(cls, ruleset: Dict[int, 'Rule'], rule: 'Rule', allow_recursion=(-1,), max_depth=3, mem={}, refMem = {}) -> 'Rule':
        if rule.i is not None and rule.i not in allow_recursion and rule.i in mem:
            # print('hit cache for', rule.i)
            return mem[rule.i]
        kind, val = rule.unpack
        if kind == 'Literal':
            mem[rule.i] = rule
            return rule
        if kind == "Ref":
            if rule in refMem:
                # print('refmem hit')
                return refMem[rule]
            if max_depth == 0:
                return Rule()
            result = cls.valid_set(ruleset, ruleset[val],  allow_recursion=allow_recursion, max_depth=max_depth - 1 if val in allow_recursion else max_depth)
            mem[rule.i] = result
            if val not in allow_recursion:
                refMem[rule] = result
            return result
        if kind == "Combo":
            result = sum((cls.valid_set(ruleset, r, allow_recursion=allow_recursion, max_depth=max_depth) for r in val), cls())
            mem[rule.i] = result
            return result
        if kind == "Union":
            children = []
            for child in val:
                subrule = cls.valid_set(ruleset, child, allow_recursion=allow_recursion, max_depth=max_depth)
                if subrule.kind == 'Union':
                    children += abs(subrule)
                else:
                    children.append(subrule)
            result = cls(union=tuple(children))
            mem[rule.i] = result
            return result
            
    @lru_cache(maxsize=None)
    def __add__(self, other):
        kind_a, val_a = self.unpack
        kind_b, val_b = other.unpack
        if kind_a == kind_b == 'Literal':
            return Rule(literal=val_a+val_b)
        if kind_a == "Nil":
            return other
        if kind_b == 'Nil':
            return self
        if kind_a == 'Literal' and kind_b =='Union':
            return Rule(union=tuple(self + subrule for subrule in val_b))
        if kind_a == 'Union' and kind_b =='Literal':
            return Rule(union=tuple(subrule + other for subrule in val_a))
        if kind_a == kind_b == 'Union':
            r = Rule(union=cross_product_sum(val_a, val_b))
            return r
        raise Exception('tried to add', self, other)

    def to_raw(self):
        if self.kind == 'Literal':
            return abs(self)
        if self.kind == 'Union':
            return set(map(lambda x: x.to_raw(), abs(self)))

def parse_rules(raw: List[str]) -> Dict[int, Rule]:
    return hashabledict(map(Rule.parse_raw, raw))

def part_one(raw_rules: List[str], messages: List[str]) -> int:
    rules = parse_rules(raw_rules)
    valid_inputs = Rule.valid_set(rules, rules[0]).to_raw()
    valid_messages = [message for message in messages if message in valid_inputs]
    return len(valid_messages)



def part_two(raw_rules: List[str], messages: List[str]) -> int:
    rules = parse_rules(raw_rules)
    rules.update(parse_rules(["8: 42 | 42 8", "11: 42 31 | 42 11 31"]))
    valid_inputs = Rule.valid_set(rules, rules[0], allow_recursion=(8, 11), max_depth=3).to_raw()
    valid_messages = [message for message in messages if message in valid_inputs]
    return len(valid_messages)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
    print("Part One: ", part_one(*get_data()))
    print("Part Two: ", part_two(*get_data()))

