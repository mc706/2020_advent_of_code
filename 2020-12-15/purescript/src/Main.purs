module Main where

import Prelude
import Data.Maybe (fromMaybe, Maybe(..))
import Data.Array (foldl, length, last, zip, (..))
import Data.Function (applyN)
import Data.Tuple
import Data.Map as Map
import Effect (Effect)
import Effect.Console (log)

data LastOccurrance
  = Once Int
  | More Int Int

type History
  = Map.Map Int LastOccurrance

type Turn
  = { turn :: Int, last :: Int, history :: History }

updateHistory :: Int -> Int -> History -> History
updateHistory i num history = case Map.lookup num history of
  Just (Once l1) -> Map.insert num (More l1 i) history
  Just (More _ l2) -> Map.insert num (More l2 i) history
  Nothing -> Map.insert num (Once i) history

setStart :: Array Int -> Turn
setStart nums =
  let
    startingHistory = foldl (\hist (Tuple i n) -> updateHistory i n hist) Map.empty (zip (0 .. (length nums)) nums)
  in
    { turn: length nums, last: last nums # fromMaybe 0, history: startingHistory }

findNext :: Turn -> Turn
findNext { turn, last, history } = case Map.lookup last history of
  Just (More l1 l2) -> { turn: turn + 1, last: l2 - l1, history: updateHistory turn (l2 - l1) history }
  _ -> { turn: turn + 1, last: 0, history: updateHistory turn 0 history }

playMemoryGame :: Array Int -> Int -> Int
playMemoryGame seed target = _.last $ applyN findNext (target - length seed) (setStart seed)

main :: Effect Unit
main = do
  log $ "Part One: " <> show (playMemoryGame [ 0, 5, 4, 1, 10, 14, 7 ] 2020)
  log $ "Part One: " <> show (playMemoryGame [ 0, 5, 4, 1, 10, 14, 7 ] 30000000)
