from typing import List

def memory_game(seed: List[int], target: int = 2020) -> int:
    """
    >>> memory_game ([0, 3, 6])
    436
    >>> memory_game([1,3,2])
    1
    >>> memory_game([2,1,3])
    10
    >>> memory_game([1,2,3])
    27
    """
    history = {}
    last = None
    for i in range(target):
        # print('starting last', last)
        if seed:
            last = seed.pop(0)
        else:
            if last in history and len(history[last]) > 1:
                apu,pu = history[last][-2:]
                last = pu - apu
            else:
                last = 0
        history.setdefault(last, []).append(i)
        # print(i+1, last)
    return last
        

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    print("test", memory_game([0,3,6], 20))
    print("Part One: ", memory_game([0,5,4,1,10,14,7]))
    print("Part Two: ", memory_game([0,5,4,1,10,14,7], 30000000))