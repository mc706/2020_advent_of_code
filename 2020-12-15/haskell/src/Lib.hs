module Lib
  ( playMemoryGame,
  )
where

import qualified Data.Map as Map

data LastOccurrance
  = Once Int
  | More Int Int

type History = Map.Map Int LastOccurrance

type Turn = (Int, Int, History)

getTurnLast :: Turn -> Int
getTurnLast (_, x, _) = x

updateHistory :: Int -> Int -> History -> History
updateHistory i num history =
  case Map.lookup num history of
    Just (Once l1) -> Map.insert num (More l1 i) history
    Just (More _ l2) -> Map.insert num (More l2 i) history
    Nothing -> Map.insert num (Once i) history

setStart :: [Int] -> Turn
setStart nums =
  let startingHistory =
        foldl (\hist (i, n) -> updateHistory i n hist) Map.empty $ zip [0 ..] nums
   in (length nums, last nums, startingHistory)

findNext :: Turn -> Turn
findNext (index, last, history) =
  case Map.lookup last history of
    Just (More l1 l2) -> (index + 1, l2 - l1, updateHistory index (l2 - l1) history)
    _ -> (index + 1, 0, updateHistory index 0 history)

playMemoryGame :: [Int] -> Int -> Int
playMemoryGame seed target =
  getTurnLast $ (!! (target - length seed)) $ iterate findNext (setStart seed)
