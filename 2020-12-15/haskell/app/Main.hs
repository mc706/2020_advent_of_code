module Main where

import Lib

main :: IO ()
main = do
  putStrLn $ "Part One ([0,5,4,1,10,14,7]) (2020): " <> show (playMemoryGame [0, 5, 4, 1, 10, 14, 7] 2020)
  putStrLn $ "Part One ([0,5,4,1,10,14,7]) (30000000): " <> show (playMemoryGame [0, 5, 4, 1, 10, 14, 7] 30000000)
